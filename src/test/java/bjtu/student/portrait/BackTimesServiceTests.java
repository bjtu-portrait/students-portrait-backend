package bjtu.student.portrait;

import bjtu.student.portrait.mapper.BackTimesMapper;
import bjtu.student.portrait.model.BackTime;
import bjtu.student.portrait.model.Statistic;
import bjtu.student.portrait.service.BackTimesService;
import bjtu.student.portrait.util.DateUtil;
import bjtu.student.portrait.util.SSHUtil;
import bjtu.student.portrait.util.Semester;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class BackTimesServiceTests {
    static {
        SSHUtil.connect();
    }

    @Autowired
    BackTimesMapper backTimesMapper;

    @Autowired
    BackTimesService backTimesService;

    @Test
    void listSemesterBackTimesByStuIdTest() {
        Semester semester = DateUtil.getSemester();
        String averageBackTime = backTimesMapper.selectSemesterAverageBackTimeByStuId(1, semester.startDate(), semester.endDate());
        List<BackTime> backTimes = backTimesMapper.listSemesterBackTimesByStuId(1, semester.startDate(), semester.endDate());
        Statistic statistic = backTimesService.getStatisticByStuNum("21178731");
        System.out.println(statistic);
    }
}
