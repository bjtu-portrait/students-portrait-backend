package bjtu.student.portrait;

import bjtu.student.portrait.service.BaseInfoService;
import bjtu.student.portrait.util.SSHUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BaseInfoServiceTests {
    static {
        SSHUtil.connect();
    }

    @Autowired
    BaseInfoService baseInfoService;

    @Test
    void getBaseInfoTest(){
        var baseInfo = baseInfoService.getBaseInfo("21169512");
        System.out.println(baseInfo);
    }

}
