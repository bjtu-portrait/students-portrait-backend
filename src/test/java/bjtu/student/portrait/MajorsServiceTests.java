package bjtu.student.portrait;

import bjtu.student.portrait.service.MajorsService;
import bjtu.student.portrait.util.SSHUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MajorsServiceTests {
    static {
        SSHUtil.connect();
    }

    @Autowired
    private MajorsService majorsService;

    @Test
    void getMajorsTest() {
        var majors = majorsService.getMajors();
        assert majors.size() != 0;
        System.out.println(majors);
    }
}
