package bjtu.student.portrait;

import bjtu.student.portrait.service.ClassesService;
import bjtu.student.portrait.util.SSHUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ClassesServiceTests {

    static {
        SSHUtil.connect();
    }

    @Autowired
    private ClassesService classesService;

    @Test
    void getClassesTest() {
        var classes = classesService.getClasses("通信工程");
        assert classes.size() != 0;
        System.out.println(classes);
        classes = classesService.getClasses("");
        assert classes.size() != 0;
        System.out.println(classes);
    }
}
