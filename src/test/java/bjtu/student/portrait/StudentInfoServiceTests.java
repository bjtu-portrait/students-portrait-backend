package bjtu.student.portrait;

import bjtu.student.portrait.dto.StudentPageDTO;
import bjtu.student.portrait.exception.NotFoundException;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.service.StudentInfoService;
import bjtu.student.portrait.util.Pack;
import bjtu.student.portrait.util.SSHUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

@SpringBootTest
public class StudentInfoServiceTests {

    static {
        SSHUtil.connect();
    }

    @Autowired
    private StudentInfoService studentInfoService;
    @Autowired
    private StudentInfoMapper studentInfoMapper;

    @Test
    void getTrueIdTest() {
        var id = studentInfoService.getTrueId("21169512");
        assertEquals(1, id);
        assertThrowsExactly(NotFoundException.class, () -> studentInfoService.getTrueId(""));
    }

    @Test
    void getStudentInfoTest() {
        var dto = StudentPageDTO.builder()
                .page(1)
                .size(10)
                .name("谢安琪")
                .stuId("")
                .major("环境工程")
                .cls("2001")
                .build();
        var student = studentInfoMapper.getStudentPage(new Page<>(dto.getPage(), dto.getSize()), dto);
        var studentPage = Pack.studentPage(student);
        assertEquals(1, studentPage.getTotal());
    }

}
