package bjtu.student.portrait;

import bjtu.student.portrait.service.StudiesService;
import bjtu.student.portrait.util.SSHUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StudiesServiceTests {
    static {
        SSHUtil.connect();
    }
    @Autowired
    StudiesService StudiesService;
    @Test
    void getStudyTest(){
        var studyInfo = StudiesService.getStudiesByStuNum("21200865");
        System.out.println(studyInfo);
    }

}
