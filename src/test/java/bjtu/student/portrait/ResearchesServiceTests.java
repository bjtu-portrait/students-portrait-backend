package bjtu.student.portrait;

import bjtu.student.portrait.service.ResearchesService;
import bjtu.student.portrait.util.SSHUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
@SpringBootTest
public class ResearchesServiceTests {

        static {
            SSHUtil.connect();
        }
        @Autowired
        ResearchesService researchesService;
        @Test
        void getResearchTest(){
            var studyInfo = researchesService.getResearchesByStuNum("19163712");
            System.out.println(studyInfo);
        }


}
