package bjtu.student.portrait.util;

import java.time.LocalDate;

public record Semester(
        LocalDate startDate,
        LocalDate endDate
) {
}
