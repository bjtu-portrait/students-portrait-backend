package bjtu.student.portrait.util;

import bjtu.student.portrait.model.Student;
import bjtu.student.portrait.model.StudentPage;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.LinkedList;
import java.util.List;

public class Pack {
    public static StudentPage studentPage(IPage<Student> page) {
        List<Student> studentList = new LinkedList<>();
        StudentPage studentPage = new StudentPage();
        studentPage.setTotal(page.getTotal());
        studentPage.setRecords(page.getRecords());
        return studentPage;
    }
}
