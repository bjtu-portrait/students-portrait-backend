package bjtu.student.portrait.util;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;

/**
 * @author Darley
 */
public class SSHUtil {

    static final String host, lHost, rHost;
    static final int port, lPort, rPort;
    static final String username, privateKeyPath;

    static {
        host = "8.130.78.125";
        lHost = "0.0.0.0";
        rHost = "localhost";
        port = 22;
        lPort = 3306;
        rPort = 3306;
        username = "portrait";
        privateKeyPath = "~/.ssh/id_rsa";
    }


    public static void connect() {
        JSch jsch = new JSch();
        try {
            jsch.addIdentity(privateKeyPath);
            var session = jsch.getSession(username, host, port);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            session.setPortForwardingL(lHost,lPort, rHost, rPort);
        } catch (JSchException ignored) {
        }
    }
}
