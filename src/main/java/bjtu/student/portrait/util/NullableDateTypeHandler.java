package bjtu.student.portrait.util;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.openapitools.jackson.nullable.JsonNullable;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@MappedJdbcTypes(JdbcType.DATE)
public class NullableDateTypeHandler extends org.apache.ibatis.type.BaseTypeHandler<JsonNullable<LocalDate>> {

    @Override
    public JsonNullable<LocalDate> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return JsonNullable.of(rs.getDate(columnName) == null ? null : rs.getDate(columnName).toLocalDate());
    }

    @Override
    public JsonNullable<LocalDate> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return JsonNullable.of(rs.getDate(columnIndex) == null ? null : rs.getDate(columnIndex).toLocalDate());
    }

    @Override
    public JsonNullable<LocalDate> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return JsonNullable.of(cs.getDate(columnIndex) == null ? null : cs.getDate(columnIndex).toLocalDate());
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, JsonNullable<LocalDate> parameter, JdbcType jdbcType) throws SQLException {
        ps.setDate(i, java.sql.Date.valueOf(parameter.get()));
    }
}
