package bjtu.student.portrait.util;

import java.time.LocalDate;

public class DateUtil {
    public static Semester getSemester() {
        // 获取当前时间
        LocalDate localDate = LocalDate.now();

        // 8月1日至1月31日前为秋季学期，2月1日至7月31日前为春季学期
        // 获取当前学期的开始日期和结束日期
        LocalDate startLocalDate;
        LocalDate endLocalDate;

        int month = localDate.getMonthValue();
        if (month >= 2 && month <= 7) {
            startLocalDate = LocalDate.of(localDate.getYear(), 2, 1);
            endLocalDate = LocalDate.of(localDate.getYear(), 7, 31);
        } else if (month >= 8) {
            startLocalDate = LocalDate.of(localDate.getYear(), 8, 1);
            endLocalDate = LocalDate.of(localDate.getYear() + 1, 1, 31);
        } else {
            startLocalDate = LocalDate.of(localDate.getYear() - 1, 8, 1);
            endLocalDate = LocalDate.of(localDate.getYear(), 1, 31);
        }
        return new Semester(startLocalDate, endLocalDate);
    }
}
