package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Parents;
import bjtu.student.portrait.model.Parent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Darley
 * @description 针对表【parents】的数据库操作Mapper
 * @createDate 2023-03-11 22:34:10
 * @Entity bjtu.student.portrait.entity.Parents
 */
@Mapper
public interface ParentsMapper extends BaseMapper<Parents> {
    Parent getMother(String stuId);

    Parent getFather(String stuId);
}




