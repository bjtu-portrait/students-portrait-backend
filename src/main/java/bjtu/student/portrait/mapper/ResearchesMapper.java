package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Researches;
import bjtu.student.portrait.model.Research;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【researches】的数据库操作Mapper
 * @createDate 2023-03-11 22:34:29
 * @Entity bjtu.student.portrait.entity.Researches
 */
@Mapper
public interface ResearchesMapper extends BaseMapper<Researches> {
    List<Research> getResearchesByStuId(Integer stuId);
}




