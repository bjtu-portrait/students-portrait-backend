package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Jobs;
import bjtu.student.portrait.model.JobTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【jobs】的数据库操作Mapper
 * @createDate 2023-03-11 22:33:59
 * @Entity bjtu.student.portrait.entity.Jobs
 */
public interface JobsMapper extends BaseMapper<Jobs> {
    List<JobTime> selectByStuIdGroupByYear(Integer stuId);

    int countNameByStuId(@Param("stuId") Integer stuId);

}




