package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Loans;
import bjtu.student.portrait.model.Loan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【loans】的数据库操作Mapper
 * @createDate 2023-03-11 22:34:05
 * @Entity bjtu.student.portrait.entity.Loans
 */
public interface LoansMapper extends BaseMapper<Loans> {
    int selectRepayAmountByStuId(Integer stuId);

    List<Loan> selectLoansByStuId(Integer stuId);
}




