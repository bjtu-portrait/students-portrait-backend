package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Punishments;
import bjtu.student.portrait.model.Punishment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【punishments】的数据库操作Mapper
 * @createDate 2023-03-11 22:34:15
 * @Entity bjtu.student.portrait.entity.Punishments
 */
public interface PunishmentsMapper extends BaseMapper<Punishments> {
    List<Punishment> selectPunishmentsByStuId(Integer stuId);
}




