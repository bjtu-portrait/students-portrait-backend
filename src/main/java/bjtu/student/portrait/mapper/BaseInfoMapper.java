package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.BaseInfo;
import bjtu.student.portrait.model.BaseInfoRes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【base_info】的数据库操作Mapper
 * @createDate 2023-03-09 18:04:36
 * @Entity bjtu.student.portrait.entity.BaseInfo
 */
@Mapper
public interface BaseInfoMapper extends BaseMapper<BaseInfo> {

    BaseInfoRes getBaseInfo(String stuId);

    List<BaseInfoRes> getBaseInfoByStuNums(List<String> stuNums);

}






