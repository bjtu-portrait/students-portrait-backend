package bjtu.student.portrait.mapper;

import bjtu.student.portrait.dto.StudentPageDTO;
import bjtu.student.portrait.entity.StudentInfo;
import bjtu.student.portrait.exception.NotFoundException;
import bjtu.student.portrait.model.Dormmate;
import bjtu.student.portrait.model.HeadInfoRes;
import bjtu.student.portrait.model.Student;
import bjtu.student.portrait.model.StudentInfoRes;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【student_info】的数据库操作Mapper
 * @createDate 2023-03-10 14:52:56
 * @Entity bjtu.student.portrait.entity.StudentInfo
 */
@Mapper
public interface StudentInfoMapper extends BaseMapper<StudentInfo> {

    default Integer getTrueId(String stuId) {
        var result = this.selectOne(new LambdaQueryWrapper<StudentInfo>().eq(StudentInfo::getStuNumber, stuId));
        if (result == null) {
            throw new NotFoundException("学号不存在");
        }
        return result.getId();
    }

    Integer selectIdByStuNumber(@Param("stuNumber") String stuNumber);

    IPage<Student> getStudentPage(IPage<?> page, StudentPageDTO dto);

    List<String> getStuNumsByStudentPage(@Param("dto") StudentPageDTO dto);

    StudentInfoRes getStudentInfo(String stuId);

    List<StudentInfoRes> getStudentInfoByStuNums(List<String> stuNums);

    HeadInfoRes getHeadInfo(String stuId);

    List<HeadInfoRes> getHeadInfoByStuNums(List<String> stuNums);

    StudentInfo selectOneByStuNumber(@Param("stuNumber") String stuNumber);

    List<Dormmate> getDormmateByStuNum(String stuNum);
}




