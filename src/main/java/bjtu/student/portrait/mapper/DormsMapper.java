package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Dorms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Darley
 * @description 针对表【dorms】的数据库操作Mapper
 * @createDate 2023-03-11 22:33:47
 * @Entity bjtu.student.portrait.entity.Dorms
 */
public interface DormsMapper extends BaseMapper<Dorms> {
    Dorms selectOneById(@Param("id") Integer id);
}




