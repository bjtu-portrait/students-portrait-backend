package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Allowances;
import bjtu.student.portrait.model.Allowance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【allowances】的数据库操作Mapper
 * @createDate 2023-03-11 22:33:09
 * @Entity bjtu.student.portrait.entity.Allowances
 */
public interface AllowancesMapper extends BaseMapper<Allowances> {
    List<Allowance> selectAllowancesByStuId(Integer stuId);
}




