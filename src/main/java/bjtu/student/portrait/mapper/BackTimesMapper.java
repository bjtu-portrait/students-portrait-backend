package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.BackTimes;
import bjtu.student.portrait.model.Average;
import bjtu.student.portrait.model.BackTime;
import bjtu.student.portrait.model.Record;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Darley
 * @description 针对表【back_times】的数据库操作Mapper
 * @createDate 2023-05-09 22:51:39
 * @Entity bjtu.student.portrait.entity.BackTimes
 */
@Mapper
public interface BackTimesMapper extends BaseMapper<BackTimes> {
    String selectSemesterAverageBackTimeByStuId(Integer stuId, LocalDate startDate, LocalDate endDate);

    List<BackTime> listSemesterBackTimesByStuId(Integer stuId, LocalDate startDate, LocalDate endDate);

    List<Average> listAveragesByStuId(Integer stuId);

    List<Record> listUnNormalRecordsByStuId(Integer stuId);
}




