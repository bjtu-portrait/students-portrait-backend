package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Scholarships;
import bjtu.student.portrait.model.Scholarship;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【scholarships】的数据库操作Mapper
 * @createDate 2023-03-11 22:34:36
 * @Entity bjtu.student.portrait.entity.Scholarships
 */
public interface ScholarshipsMapper extends BaseMapper<Scholarships> {
    List<Scholarship> selectScholarshipsByStuId(Integer stuId);
}




