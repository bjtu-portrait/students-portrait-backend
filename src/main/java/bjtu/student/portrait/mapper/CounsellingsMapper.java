package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Counsellings;
import bjtu.student.portrait.model.Counselling;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Darley
 * @description 针对表【counsellings】的数据库操作Mapper
 * @createDate 2023-03-11 22:33:37
 * @Entity bjtu.student.portrait.entity.Counsellings
 */
public interface CounsellingsMapper extends BaseMapper<Counsellings> {
    Integer selectSemesterAmountByStuId(Integer stuId, LocalDate startDate, LocalDate endDate);
    List<Counselling> listCounsellingsByStuId(Integer stuId);

}




