package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Majors;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【majors】的数据库操作Mapper
 * @createDate 2023-04-08 22:45:16
 * @Entity bjtu.student.portrait.entity.Majors
 */
@Mapper
public interface MajorsMapper extends BaseMapper<Majors> {
    List<String> getMajors();

    List<Integer> getIdsByName(String name);
}




