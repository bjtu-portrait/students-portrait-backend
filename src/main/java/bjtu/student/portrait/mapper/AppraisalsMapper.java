package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Appraisals;
import bjtu.student.portrait.model.Appraisal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【appraisals】的数据库操作Mapper
 * @createDate 2023-03-11 22:33:20
 * @Entity bjtu.student.portrait.entity.Appraisals
 */
@Mapper
public interface AppraisalsMapper extends BaseMapper<Appraisals> {
    List<Appraisal> listAppraisalsByDormId(@Param("dormId") Integer dormId);
}




