package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Activities;
import bjtu.student.portrait.model.Activity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Darley
 * @description 针对表【activities】的数据库操作Mapper
 * @createDate 2023-03-11 22:30:46
 * @Entity bjtu.student.portrait.entity.Activities
 */
public interface ActivitiesMapper extends BaseMapper<Activities> {
    Integer selectSemesterAmountByStuId(Integer stuId, LocalDate startDate, LocalDate endDate);

    Integer selectSemesterTallyByStuId(Integer stuId, LocalDate startDate, LocalDate endDate);

    List<Activity> listActivitiesByStuId(Integer stuId);
}




