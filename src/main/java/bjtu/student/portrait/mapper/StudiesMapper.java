package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Studies;
import bjtu.student.portrait.model.Study;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【studies】的数据库操作Mapper
 * @createDate 2023-03-11 22:34:47
 * @Entity bjtu.student.portrait.entity.Studies
 */
@Mapper
public interface StudiesMapper extends BaseMapper<Studies> {
   List<Study> getStudiesByStuId(Integer stuId);

}




