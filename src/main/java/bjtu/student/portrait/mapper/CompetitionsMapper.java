package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Competitions;
import bjtu.student.portrait.model.Competition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【competitions】的数据库操作Mapper
 * @createDate 2023-03-11 22:33:32
 * @Entity bjtu.student.portrait.entity.Competitions
 */
public interface CompetitionsMapper extends BaseMapper<Competitions> {
    List<Competition> selectCompetitionsByStuId(Integer stuId);
}




