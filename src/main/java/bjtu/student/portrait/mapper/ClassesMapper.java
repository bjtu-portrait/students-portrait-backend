package bjtu.student.portrait.mapper;

import bjtu.student.portrait.entity.Classes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【classes】的数据库操作Mapper
 * @createDate 2023-04-08 22:44:53
 * @Entity bjtu.student.portrait.entity.Classes
 */
public interface ClassesMapper extends BaseMapper<Classes> {
    List<String> getClasses(List<Integer> majorIds);
}




