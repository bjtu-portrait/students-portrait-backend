package bjtu.student.portrait;

import bjtu.student.portrait.util.SSHUtil;
import com.fasterxml.jackson.databind.Module;
import org.mybatis.spring.annotation.MapperScan;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;

@SpringBootApplication(
    nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
@ComponentScan(
    basePackages = {"bjtu.student.portrait", "bjtu.student.portrait.api" , "bjtu.student.portrait.config"},
    nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
@MapperScan("bjtu.student.portrait.mapper")
public class Application {

    public static void main(String[] args) {
        SSHUtil.connect();
        SpringApplication.run(Application.class, args);
    }

    @Bean(name = "bjtu.student.portrait.Application.jsonNullableModule")
    public Module jsonNullableModule() {
        return new JsonNullableModule();
    }

}
