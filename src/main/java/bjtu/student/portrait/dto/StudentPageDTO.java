package bjtu.student.portrait.dto;

public class StudentPageDTO {
    private final Integer page;
    private final Integer size;
    private final String name;
    private final String stuId;
    private final String major;
    private final String cls;

    private StudentPageDTO(Builder builder) {
        page = builder.page;
        size = builder.size;
        name = builder.name;
        stuId = builder.stuId;
        major = builder.major;
        cls = builder.cls;
    }

    public static Builder builder() {
        return new Builder();
    }


    public Integer getPage() {
        return page;
    }

    public Integer getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public String getStuId() {
        return stuId;
    }

    public String getMajor() {
        return major;
    }

    public String getCls() {
        return cls;
    }

    @Override
    public String toString() {
        return "StudentPageDTO{" +
                "page=" + page +
                ", size=" + size +
                ", name='" + name + '\'' +
                ", stuId='" + stuId + '\'' +
                ", major='" + major + '\'' +
                ", cls='" + cls + '\'' +
                '}';
    }

    public static final class Builder {
        private Integer page;
        private Integer size;
        private String name;
        private String stuId;
        private String major;
        private String cls;

        private Builder() {
        }

        public Builder page(Integer page) {
            this.page = page;
            return this;
        }

        public Builder size(Integer size) {
            this.size = size;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder stuId(String stuId) {
            this.stuId = stuId;
            return this;
        }

        public Builder major(String major) {
            this.major = major;
            return this;
        }

        public Builder cls(String cls) {
            this.cls = cls;
            return this;
        }

        public StudentPageDTO build() {
            return new StudentPageDTO(this);
        }
    }
}
