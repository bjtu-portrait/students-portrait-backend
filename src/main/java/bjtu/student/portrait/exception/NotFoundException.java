package bjtu.student.portrait.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ApiException {

    public NotFoundException(HttpStatus status, String msg) {
        super(status, msg);
    }

    public NotFoundException(String msg) {
        super(HttpStatus.NOT_FOUND, msg);
    }
}
