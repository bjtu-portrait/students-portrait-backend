package bjtu.student.portrait.exception;

import org.springframework.http.HttpStatus;

public class ApiException extends RuntimeException {
    private final int code;

    public ApiException(HttpStatus status, String msg) {
        super(msg);
        this.code = status.value();
    }

    public int getCode() {
        return code;
    }
}
