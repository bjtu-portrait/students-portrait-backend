package bjtu.student.portrait.exception;

import bjtu.student.portrait.model.ErrorRes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

// 使用 @RestController 时，返回对象会被自动封装为 ResponseEntity，默认的状态码为 200
// 使用 @Controller 时，在方法上加上 @ResponseBody，返回对象会被自动封装为 ResponseEntity，默认的状态码为 200
// 两者皆可以在方法上使用 @ResponseStatus 自定义状态码，皆可以返回自定义构造的 ResponseEntity
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = ApiException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorRes apiExceptionHandler(ApiException e) {
        return new ErrorRes(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(value = IOException.class)
    @ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
    public ErrorRes ioExceptionHandler(IOException e) {
        return new ErrorRes(500, "服务器错误");
    }
}
