package bjtu.student.portrait.api;

import bjtu.student.portrait.model.Research;
import bjtu.student.portrait.model.Study;
import bjtu.student.portrait.service.ResearchesService;
import bjtu.student.portrait.service.StudiesService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@Controller
public class StudyApiController implements StudyApi {
    private final NativeWebRequest request;
    private final StudiesService studiesService;
    private final ResearchesService researchesService;

    @Autowired
    public StudyApiController(
            NativeWebRequest request,
            StudiesService studiesService,
            ResearchesService researchesService
    ) {
        this.request = request;
        this.studiesService = studiesService;
        this.researchesService = researchesService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<List<Study>> getStudies(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(studiesService.getStudiesByStuNum(stuId));
    }

    @Override
    public ResponseEntity<List<Research>> getResearches(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(researchesService.getResearchesByStuNum(stuId));
    }
}
