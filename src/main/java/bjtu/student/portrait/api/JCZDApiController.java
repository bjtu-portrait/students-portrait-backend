package bjtu.student.portrait.api;

import bjtu.student.portrait.model.*;
import bjtu.student.portrait.service.*;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@Controller
public class JCZDApiController implements JCZDApi {
    private final NativeWebRequest request;
    private final AllowancesService allowancesService;
    private final ScholarshipsService scholarshipsService;
    private final CompetitionsService competitionsService;
    private final LoansService loansService;
    private final PunishmentsService punishmentsService;
    private final JobsService jobsService;

    @Autowired
    public JCZDApiController(
            NativeWebRequest request,
            AllowancesService allowancesService,
            ScholarshipsService scholarshipsService,
            CompetitionsService competitionsService,
            LoansService loansService,
            PunishmentsService punishmentsService,
            JobsService jobsService
    ) {
        this.request = request;
        this.allowancesService = allowancesService;
        this.scholarshipsService = scholarshipsService;
        this.competitionsService = competitionsService;
        this.loansService = loansService;
        this.punishmentsService = punishmentsService;
        this.jobsService = jobsService;

    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<List<Scholarship>> getScholarships(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(scholarshipsService.getScholarshipsByStuNum(stuId));
    }

    @Override
    public ResponseEntity<List<Competition>> getCompetitions(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(competitionsService.getCompetitionsByStuNum(stuId));
    }

    @Override
    public ResponseEntity<List<Allowance>> getAllowances(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(allowancesService.getAllowancesByStuNum(stuId));
    }

    @Override
    public ResponseEntity<LoanRes> getLoan(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(loansService.getLoanByStuNum(stuId));
    }

    @Override
    public ResponseEntity<List<Punishment>> getPunishments(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(punishmentsService.getPunishmentsByStuNum(stuId));
    }

    @Override
    public ResponseEntity<Job> getJob(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(jobsService.getJobByStuNum(stuId));
    }
}

