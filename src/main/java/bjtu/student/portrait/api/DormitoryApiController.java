package bjtu.student.portrait.api;

import bjtu.student.portrait.model.Dorm;
import bjtu.student.portrait.model.Dormmate;
import bjtu.student.portrait.model.Statistic;
import bjtu.student.portrait.service.BackTimesService;
import bjtu.student.portrait.service.DormsService;
import bjtu.student.portrait.service.StudentInfoService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@Controller
public class DormitoryApiController implements DormitoryApi {
    private final NativeWebRequest request;
    private final DormsService dormsService;
    private final StudentInfoService studentInfoService;
    private final BackTimesService backTimesService;

    @Autowired
    public DormitoryApiController(
            NativeWebRequest request,
            DormsService dormsService,
            StudentInfoService studentInfoService,
            BackTimesService backTimesService
    ) {
        this.request = request;
        this.dormsService = dormsService;
        this.studentInfoService = studentInfoService;
        this.backTimesService = backTimesService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<Dorm> getDorm(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(dormsService.getDormByStuNum(stuId));
    }

    @Override
    public ResponseEntity<List<Dormmate>> getDormmates(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(studentInfoService.getDormmateByStuNum(stuId));
    }

    @Override
    public ResponseEntity<Statistic> getStatistic(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(backTimesService.getStatisticByStuNum(stuId));
    }

}
