package bjtu.student.portrait.api;


import bjtu.student.portrait.service.ClassesService;
import jakarta.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
@Controller
@RequestMapping("${openapi.student-portrait.base-path:}")
public class ClassesApiController implements ClassesApi {

    private final NativeWebRequest request;
    private final ClassesService classesService;

    @Autowired
    public ClassesApiController(NativeWebRequest request, ClassesService classesService) {
        this.request = request;
        this.classesService = classesService;
    }


    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<List<String>> getClasses(String major) {
        return ResponseEntity.ok(classesService.getClasses(major));
    }
}
