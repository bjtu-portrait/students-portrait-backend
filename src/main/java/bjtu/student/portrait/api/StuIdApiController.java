package bjtu.student.portrait.api;

import bjtu.student.portrait.exception.ApiException;
import bjtu.student.portrait.model.StuIdRes;
import bjtu.student.portrait.service.StudentInfoService;
import jakarta.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
@Controller
@RequestMapping("${openapi.student-portrait.base-path:}")
public class StuIdApiController implements StuIdApi {

    private final NativeWebRequest request;
    private final StudentInfoService studentInfoService;

    @Autowired
    public StuIdApiController(NativeWebRequest request, StudentInfoService studentInfoService) {
        this.request = request;
        this.studentInfoService = studentInfoService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<StuIdRes> getStuId(String name) {
        if (name.isBlank())
            throw new ApiException(HttpStatus.BAD_REQUEST, "name is empty");
        var res = studentInfoService.getIdByName(name);
        return ResponseEntity.ok(res);
    }

}
