package bjtu.student.portrait.api;

import bjtu.student.portrait.model.ErrorRes;
import bjtu.student.portrait.model.Research;
import bjtu.student.portrait.model.Study;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

public interface StudyApi {
    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /student/{stuId}/study/studies : 学习
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     * or 参数不正确 (status code 400)
     */
    @Operation(
            operationId = "getStudies",
            summary = "学习",
            description = "",
            tags = {"学习科研"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Study.class)))
                    }),
                    @ApiResponse(responseCode = "400", description = "参数不正确", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorRes.class))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/study/studies",
            produces = {"application/json"}
    )
    default ResponseEntity<List<Study>> getStudies(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"isRestudy\" : true, \"score\" : 6.0274563, \"lesson\" : \"lesson\", \"semester\" : \"semester\", \"credit\" : 0.8008282 }, { \"isRestudy\" : true, \"score\" : 6.0274563, \"lesson\" : \"lesson\", \"semester\" : \"semester\", \"credit\" : 0.8008282 } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /student/{stuId}/study/researches : 科研
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getResearches",
            summary = "科研",
            description = "",
            tags = {"学习科研"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Research.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/study/researches",
            produces = {"application/json"}
    )
    default ResponseEntity<List<Research>> getResearches(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"name\" : \"name\", \"type\" : 0 }, { \"date\" : \"2000-01-23\", \"name\" : \"name\", \"type\" : 0 } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }
}
