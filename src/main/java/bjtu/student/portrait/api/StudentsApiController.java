package bjtu.student.portrait.api;

import bjtu.student.portrait.dto.StudentPageDTO;
import bjtu.student.portrait.model.ExportRes;
import bjtu.student.portrait.model.StudentPage;
import bjtu.student.portrait.service.StudentInfoService;
import com.alibaba.excel.EasyExcel;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import jakarta.annotation.Generated;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
@Controller
@RequestMapping("${openapi.student-portrait.base-path:}")
public class StudentsApiController implements StudentsApi {

    private final NativeWebRequest request;
    private final StudentInfoService studentInfoService;

    @Autowired
    public StudentsApiController(NativeWebRequest request, StudentInfoService studentInfoService) {
        this.request = request;
        this.studentInfoService = studentInfoService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<StudentPage> getStudents(Integer page, Integer size, String name, String stuid, String major, String cls) {
        var studentPageDTO = StudentPageDTO.builder().page(page).size(size).name(name).stuId(stuid).major(major).cls(cls).build();
        var studentPage = studentInfoService.getStudentPage(studentPageDTO);
        return ResponseEntity.ok(studentPage);
    }

    @Override
    public void export(
            HttpServletResponse response,
            @NotNull @Parameter(name = "name", description = "", required = true, in = ParameterIn.QUERY) @Valid @RequestParam(value = "name", required = true) String name,
            @NotNull @Parameter(name = "stunum", description = "", required = true, in = ParameterIn.QUERY) @Valid @RequestParam(value = "stunum", required = true) String stuNum,
            @NotNull @Parameter(name = "major", description = "", required = true, in = ParameterIn.QUERY) @Valid @RequestParam(value = "major", required = true) String major,
            @NotNull @Parameter(name = "cls", description = "", required = true, in = ParameterIn.QUERY) @Valid @RequestParam(value = "cls", required = true) String cls
    ) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码
        String fileName = URLEncoder.encode("学生信息" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")), StandardCharsets.UTF_8).replaceAll("\\+", "%20");
        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        var studentPageDTO = StudentPageDTO.builder().name(name).stuId(stuNum).major(major).cls(cls).build();
        var stuNums = studentInfoService.getStudentNumsByStudentPage(studentPageDTO);
        var exportData = studentInfoService.getExportData(stuNums);
        EasyExcel.write(response.getOutputStream(), ExportRes.class).sheet("Sheet1").doWrite(exportData);
    }
}
