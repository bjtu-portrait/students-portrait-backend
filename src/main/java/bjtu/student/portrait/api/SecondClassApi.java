package bjtu.student.portrait.api;

import bjtu.student.portrait.model.Activity;
import bjtu.student.portrait.model.ActivityRes;
import bjtu.student.portrait.model.Counselling;
import bjtu.student.portrait.model.CounsellingRes;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

public interface SecondClassApi {
    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /student/{stuId}/second/activity : 活动成长
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getActivity",
            summary = "活动成长",
            description = "",
            tags = {"第二课堂"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Activity.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/second/activity",
            produces = {"application/json"}
    )
    default ResponseEntity<ActivityRes> getActivity(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"name\" : \"name\", \"type\" : \"type\", \"tally\" : 0 }, { \"date\" : \"2000-01-23\", \"name\" : \"name\", \"type\" : \"type\", \"tally\" : 0 } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /student/{stuId}/second/counselling : 深度辅导
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getCounselling",
            summary = "深度辅导",
            description = "",
            tags = {"第二课堂"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Counselling.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/second/counselling",
            produces = {"application/json"}
    )
    default ResponseEntity<CounsellingRes> getCounselling(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"mentor\" : \"mentor\", \"detail\" : \"detail\" }, { \"date\" : \"2000-01-23\", \"mentor\" : \"mentor\", \"detail\" : \"detail\" } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }
}
