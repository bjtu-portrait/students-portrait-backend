package bjtu.student.portrait.api;

import bjtu.student.portrait.entity.Parents;
import bjtu.student.portrait.model.BaseInfoRes;
import bjtu.student.portrait.model.HeadInfoRes;
import bjtu.student.portrait.model.ParentsInfo;
import bjtu.student.portrait.model.StudentInfoRes;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

public interface InformationApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /student/{stuId}/information/head : 头部信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getHeadInfo",
            summary = "头部信息",
            description = "",
            tags = {"个人信息"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = HeadInfoRes.class))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/information/head",
            produces = {"application/json"}
    )
    default ResponseEntity<HeadInfoRes> getHeadInfo(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"inDorm\" : true, \"photoUrl\" : \"https://openapi-generator.tech\", \"mentor\" : \"mentor\", \"teacher\" : \"teacher\", \"inSchool\" : true, \"inNormal\" : true, \"inArchive\" : \"1\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /student/{stuId}/information/base : 基础信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getBaseInfo",
            summary = "基础信息",
            description = "",
            tags = {"个人信息"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = BaseInfoRes.class))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/information/base",
            produces = {"application/json"}
    )
    default ResponseEntity<BaseInfoRes> getBaseInfo(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"birthday\" : \"2000-01-23\", \"ancestry\" : \"ancestry\", \"address\" : \"address\", \"gender\" : \"1\", \"nation\" : \"nation\", \"phone\" : \"phone\", \"org\" : \"org\", \"name\" : \"name\", \"political\" : \"1\", \"id\" : \"id\", \"married\" : true }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /student/{stuId}/information/student : 在校信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getStudentInfo",
            summary = "在校信息",
            description = "",
            tags = {"个人信息"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = StudentInfoRes.class))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/information/student",
            produces = {"application/json"}
    )
    default ResponseEntity<StudentInfoRes> getStudentInfo(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"stu_id\" : \"stu_id\", \"major\" : \"major\", \"school\" : \"school\", \"grad_date\" : \"2000-01-23\", \"grade\" : \"1\", \"source\" : \"1\", \"class\" : \"class\", \"span\" : 0, \"entry_date\" : \"2000-01-23\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /student/{stuId}/information/parents : 父母信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getParentsInfo",
            summary = "父母信息",
            description = "",
            tags = {"个人信息"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ParentsInfo.class))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/information/parents",
            produces = {"application/json"}
    )
    default ResponseEntity<ParentsInfo> getParentsInfo(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"mother\" : { \"career\" : \"career\", \"phone\" : \"phone\", \"name\" : \"name\", \"office\" : \"office\" }, \"father\" : { \"career\" : \"career\", \"phone\" : \"phone\", \"name\" : \"name\", \"office\" : \"office\" } }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }
}
