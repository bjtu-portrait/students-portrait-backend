package bjtu.student.portrait.api;

import bjtu.student.portrait.model.BaseInfoRes;
import bjtu.student.portrait.model.HeadInfoRes;
import bjtu.student.portrait.model.ParentsInfo;
import bjtu.student.portrait.model.StudentInfoRes;
import bjtu.student.portrait.service.BaseInfoService;
import bjtu.student.portrait.service.ParentsService;
import bjtu.student.portrait.service.StudentInfoService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@Controller
public class InformationController implements InformationApi {

    private final NativeWebRequest request;
    private final StudentInfoService studentInfoService;
    private final BaseInfoService baseInfoService;
    private final ParentsService parentsService;

    @Autowired
    public InformationController(
            NativeWebRequest request,
            StudentInfoService studentInfoService,
            BaseInfoService baseInfoService,
            ParentsService parentsService
    ) {
        this.request = request;
        this.studentInfoService = studentInfoService;
        this.baseInfoService = baseInfoService;
        this.parentsService = parentsService;
    }


    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<HeadInfoRes> getHeadInfo(@PathVariable("stuId") String stuId) {
        return ResponseEntity.ok(studentInfoService.getHeadInfo(stuId));
    }

    @Override
    public ResponseEntity<StudentInfoRes> getStudentInfo(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(studentInfoService.getStudentInfo(stuId));
    }

    @Override
    public ResponseEntity<BaseInfoRes> getBaseInfo(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(baseInfoService.getBaseInfo(stuId));
    }

    @Override
    public ResponseEntity<ParentsInfo> getParentsInfo(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(parentsService.getParents(stuId));
    }

}
