package bjtu.student.portrait.api;

import bjtu.student.portrait.model.Dorm;
import bjtu.student.portrait.model.Dormmate;
import bjtu.student.portrait.model.Statistic;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

public interface DormitoryApi {
    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /student/{stuId}/dormitory/dorm : 住宿信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getDorm",
            summary = "住宿信息",
            description = "",
            tags = {"书院信息"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Dorm.class))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/dormitory/dorm",
            produces = {"application/json"}
    )
    default ResponseEntity<Dorm> getDorm(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"building\" : \"building\", \"room\" : \"room\", \"appraisals\" : [ { \"date\" : \"2000-01-23\", \"detail\" : \"detail\", \"type\" : \"1\" }, { \"date\" : \"2000-01-23\", \"detail\" : \"detail\", \"type\" : \"1\" } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /student/{stuId}/dormitory/dormmates : 室友信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getDormmates",
            summary = "室友信息",
            description = "",
            tags = {"书院信息"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Dormmate.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/dormitory/dormmates",
            produces = {"application/json"}
    )
    default ResponseEntity<List<Dormmate>> getDormmates(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"ancestry\" : \"ancestry\", \"stuId\" : \"stuId\", \"name\" : \"name\", \"class\" : \"class\" }, { \"ancestry\" : \"ancestry\", \"stuId\" : \"stuId\", \"name\" : \"name\", \"class\" : \"class\" } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /student/{stuId}/dormitory/statistic : 归寝信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getStatistic",
            summary = "归寝信息",
            description = "",
            tags = {"书院信息"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Statistic.class))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/dormitory/statistic",
            produces = {"application/json"}
    )
    default ResponseEntity<Statistic> getStatistic(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"average\" : \"average\", \"times\" : [ { \"date\" : \"2000-01-23\", \"time\" : \"time\" }, { \"date\" : \"2000-01-23\", \"time\" : \"time\" } ], \"records\" : [ { \"date\" : \"2000-01-23\", \"time\" : \"time\", \"type\" : \"1\" }, { \"date\" : \"2000-01-23\", \"time\" : \"time\", \"type\" : \"1\" } ], \"averages\" : [ { \"other\" : \"other\", \"self\" : \"self\", \"semester\" : \"semester\" }, { \"other\" : \"other\", \"self\" : \"self\", \"semester\" : \"semester\" } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
