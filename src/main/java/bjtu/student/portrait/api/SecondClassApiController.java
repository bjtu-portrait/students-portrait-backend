package bjtu.student.portrait.api;

import bjtu.student.portrait.model.ActivityRes;
import bjtu.student.portrait.model.CounsellingRes;
import bjtu.student.portrait.service.ActivitiesService;
import bjtu.student.portrait.service.CounsellingsService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@Controller
public class SecondClassApiController implements SecondClassApi {
    private final NativeWebRequest request;
    private final ActivitiesService activitiesService;
    private final CounsellingsService counsellingsService;

    @Autowired
    public SecondClassApiController(
            NativeWebRequest request,
            ActivitiesService activitiesService,
            CounsellingsService counsellingsService
    ) {
        this.request = request;
        this.activitiesService = activitiesService;
        this.counsellingsService = counsellingsService;
    }
    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }
    @Override
    public ResponseEntity<ActivityRes> getActivity(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(activitiesService.getActivityByStuNum(stuId));
    }

    @Override
    public ResponseEntity<CounsellingRes> getCounselling(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        return ResponseEntity.ok(counsellingsService.getCounsellingByStuNum(stuId));
    }

}
