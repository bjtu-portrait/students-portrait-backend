package bjtu.student.portrait.api;

import bjtu.student.portrait.model.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

public interface JCZDApi {
    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /student/{stuId}/jczd/allowances : 助学金
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getAllowances",
            summary = "助学金",
            description = "",
            tags = {"奖惩助贷"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Allowance.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/jczd/allowances",
            produces = {"application/json"}
    )
    default ResponseEntity<List<Allowance>> getAllowances(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"level\" : \"1\", \"bonus\" : 0, \"name\" : \"name\" }, { \"date\" : \"2000-01-23\", \"level\" : \"1\", \"bonus\" : 0, \"name\" : \"name\" } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /student/{stuId}/jczd/competitions : 竞赛获奖
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getCompetitions",
            summary = "竞赛获奖",
            description = "",
            tags = {"奖惩助贷"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Competition.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/jczd/competitions",
            produces = {"application/json"}
    )
    default ResponseEntity<List<Competition>> getCompetitions(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"level\" : \"1\", \"name\" : \"name\", \"department\" : \"department\" }, { \"date\" : \"2000-01-23\", \"level\" : \"1\", \"name\" : \"name\", \"department\" : \"department\" } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /student/{stuId}/jczd/job : 勤工助学
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getJob",
            summary = "勤工助学",
            description = "",
            tags = {"奖惩助贷"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Job.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/jczd/job",
            produces = {"application/json"}
    )
    default ResponseEntity<Job> getJob(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"year\" : 0, \"jobs\" : [ { \"hours\" : 6, \"name\" : \"name\" }, { \"hours\" : 6, \"name\" : \"name\" } ] }, { \"year\" : 0, \"jobs\" : [ { \"hours\" : 6, \"name\" : \"name\" }, { \"hours\" : 6, \"name\" : \"name\" } ] } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /student/{stuId}/jczd/loan : 助学贷
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getLoans",
            summary = "助学贷",
            description = "",
            tags = {"奖惩助贷"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Loan.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/jczd/loan",
            produces = {"application/json"}
    )
    default ResponseEntity<LoanRes> getLoan(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"amount\" : 0 }, { \"date\" : \"2000-01-23\", \"amount\" : 0 } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /student/{stuId}/jczd/punishments : 惩处信息
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getPunishments",
            summary = "惩处信息",
            description = "",
            tags = {"奖惩助贷"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Punishment.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/jczd/punishments",
            produces = {"application/json"}
    )
    default ResponseEntity<List<Punishment>> getPunishments(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"reason\" : \"reason\", \"type\" : \"1\" }, { \"date\" : \"2000-01-23\", \"reason\" : \"reason\", \"type\" : \"1\" } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /student/{stuId}/jczd/scholarships : 奖学金
     *
     * @param stuId (required)
     * @return 成功 (status code 200)
     */
    @Operation(
            operationId = "getScholarships",
            summary = "奖学金",
            description = "",
            tags = {"奖惩助贷"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "成功", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Scholarship.class)))
                    })
            }
    )
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/student/{stuId}/jczd/scholarships",
            produces = {"application/json"}
    )
    default ResponseEntity<List<Scholarship>> getScholarships(
            @Parameter(name = "stuId", description = "", required = true, in = ParameterIn.PATH) @PathVariable("stuId") String stuId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "[ { \"date\" : \"2000-01-23\", \"level\" : \"1\", \"bonus\" : 0, \"name\" : \"name\" }, { \"date\" : \"2000-01-23\", \"level\" : \"1\", \"bonus\" : 0, \"name\" : \"name\" } ]";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }
}
