package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class StudentPage {
    private List<Student> records;
    private long total;

    @JsonProperty("records")
    public List<Student> getRecords() {
        return records;
    }

    @JsonProperty("records")
    public void setRecords(List<Student> value) {
        this.records = value;
    }

    @JsonProperty("total")
    public long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(long value) {
        this.total = value;
    }
}
