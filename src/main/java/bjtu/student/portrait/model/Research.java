package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Research
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Research {

  @JsonProperty("name")
  private String name;

  /**
   * Get name
   * @return name
  */
  @NotNull
  @Schema(name = "name", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getName() {
    return name;
  }

  @JsonProperty("type")
  private TypeEnum type;

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate date;

  public Research name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get type
   * @return type
  */
  @NotNull
  @Schema(name = "type", requiredMode = Schema.RequiredMode.REQUIRED)
  public TypeEnum getType() {
    return type;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Research type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * Get date
   * @return date
  */
  @NotNull
  @Valid
  @Schema(name = "date", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getDate() {
    return date;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Research date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
    PAPER("论文"),

    BOOK("著作"),

    PATENT("专利"),

    SOFTWARE("软著");

    private final String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @JsonCreator
    public static TypeEnum fromValue(String value) {
      for (TypeEnum b : TypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonValue
    public String getValue() {
      return value;
    }
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Research research = (Research) o;
    return Objects.equals(this.name, research.name) &&
        Objects.equals(this.type, research.type) &&
        Objects.equals(this.date, research.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, type, date);
  }

  @Override
  public String toString() {
    String sb = "class Research {\n" +
            "    name: " + toIndentedString(name) + "\n" +
            "    type: " + toIndentedString(type) + "\n" +
            "    date: " + toIndentedString(date) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

