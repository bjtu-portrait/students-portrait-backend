package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.constraints.NotNull;

import java.util.Objects;

/**
 * JobTime
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class JobTime {

  @JsonProperty("year")
  private Integer year;

  @JsonProperty("hours")
  private Integer hours;

  public JobTime year(Integer year) {
    this.year = year;
    return this;
  }

  /**
   * Get year
   *
   * @return year
   */
  @NotNull
  @Schema(name = "year", requiredMode = Schema.RequiredMode.REQUIRED)
  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public JobTime hours(Integer hours) {
    this.hours = hours;
    return this;
  }

  /**
   * Get hours
   * @return hours
   */
  @NotNull
  @Schema(name = "hours", requiredMode = Schema.RequiredMode.REQUIRED)
  public Integer getHours() {
    return hours;
  }

  public void setHours(Integer hours) {
    this.hours = hours;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JobTime jobTime = (JobTime) o;
    return Objects.equals(this.year, jobTime.year) &&
            Objects.equals(this.hours, jobTime.hours);
  }

  @Override
  public int hashCode() {
    return Objects.hash(year, hours);
  }

  @Override
  public String toString() {
    String sb = "class JobTime {\n" +
            "    year: " + toIndentedString(year) + "\n" +
            "    hours: " + toIndentedString(hours) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

