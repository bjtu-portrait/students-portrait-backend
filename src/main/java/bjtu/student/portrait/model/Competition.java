package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Competition
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Competition {

  @JsonProperty("name")
  private String name;

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate date;

  /**
   * Get name
   * @return name
  */
  @NotNull
  @Schema(name = "name", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getName() {
    return name;
  }

  @JsonProperty("level")
  private LevelEnum level;

  @JsonProperty("department")
  private String department;

  public Competition name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get date
   * @return date
  */
  @NotNull
  @Valid
  @Schema(name = "date", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getDate() {
    return date;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Competition date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Get level
   * @return level
  */
  @NotNull
  @Schema(name = "level", requiredMode = Schema.RequiredMode.REQUIRED)
  public LevelEnum getLevel() {
    return level;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Competition level(LevelEnum level) {
    this.level = level;
    return this;
  }

  /**
   * Get department
   * @return department
  */
  @NotNull
  @Schema(name = "department", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getDepartment() {
    return department;
  }

  public void setLevel(LevelEnum level) {
    this.level = level;
  }

  public Competition department(String department) {
    this.department = department;
    return this;
  }

  /**
   * Gets or Sets level
   */
  public enum LevelEnum {
      WORLD("世界级"),

      NATION("国家级"),

      PROVINCE("省市级"),

      SCHOOL("校级");

      private final String value;

      LevelEnum(String value) {
          this.value = value;
      }

      @JsonValue
      public String getValue() {
          return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static LevelEnum fromValue(String value) {
      for (LevelEnum b : LevelEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Competition competition = (Competition) o;
    return Objects.equals(this.name, competition.name) &&
        Objects.equals(this.date, competition.date) &&
        Objects.equals(this.level, competition.level) &&
        Objects.equals(this.department, competition.department);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, date, level, department);
  }

  @Override
  public String toString() {
    String sb = "class Competition {\n" +
            "    name: " + toIndentedString(name) + "\n" +
            "    date: " + toIndentedString(date) + "\n" +
            "    level: " + toIndentedString(level) + "\n" +
            "    department: " + toIndentedString(department) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

