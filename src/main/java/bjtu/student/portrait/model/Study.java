package bjtu.student.portrait.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * Study
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Study {

  @JsonProperty("semester")
  private String semester;

  @JsonProperty("lesson")
  private String lesson;

  @JsonProperty("credit")
  private Float credit;

  @JsonProperty("score")
  private Float score;

  @JsonProperty("isRestudy")
  private Boolean isRestudy;

  public Study semester(String semester) {
    this.semester = semester;
    return this;
  }

  /**
   * Get semester
   * @return semester
  */
  @NotNull 
  @Schema(name = "semester", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getSemester() {
    return semester;
  }

  public void setSemester(String semester) {
    this.semester = semester;
  }

  public Study lesson(String lesson) {
    this.lesson = lesson;
    return this;
  }

  /**
   * Get lesson
   * @return lesson
  */
  @NotNull 
  @Schema(name = "lesson", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getLesson() {
    return lesson;
  }

  public void setLesson(String lesson) {
    this.lesson = lesson;
  }

  public Study credit(Float credit) {
    this.credit = credit;
    return this;
  }

  /**
   * Get credit
   * @return credit
  */
  @NotNull 
  @Schema(name = "credit", requiredMode = Schema.RequiredMode.REQUIRED)
  public Float getCredit() {
    return credit;
  }

  public void setCredit(Float credit) {
    this.credit = credit;
  }

  public Study score(Float score) {
    this.score = score;
    return this;
  }

  /**
   * Get score
   * @return score
  */
  @NotNull 
  @Schema(name = "score", requiredMode = Schema.RequiredMode.REQUIRED)
  public Float getScore() {
    return score;
  }

  public void setScore(Float score) {
    this.score = score;
  }

  public Study isRestudy(Boolean isRestudy) {
    this.isRestudy = isRestudy;
    return this;
  }

  /**
   * Get isRestudy
   * @return isRestudy
  */
  @NotNull 
  @Schema(name = "isRestudy", requiredMode = Schema.RequiredMode.REQUIRED)
  public Boolean getIsRestudy() {
    return isRestudy;
  }

  public void setIsRestudy(Boolean isRestudy) {
    this.isRestudy = isRestudy;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Study study = (Study) o;
    return Objects.equals(this.semester, study.semester) &&
        Objects.equals(this.lesson, study.lesson) &&
        Objects.equals(this.credit, study.credit) &&
        Objects.equals(this.score, study.score) &&
        Objects.equals(this.isRestudy, study.isRestudy);
  }

  @Override
  public int hashCode() {
    return Objects.hash(semester, lesson, credit, score, isRestudy);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Study {\n");
    sb.append("    semester: ").append(toIndentedString(semester)).append("\n");
    sb.append("    lesson: ").append(toIndentedString(lesson)).append("\n");
    sb.append("    credit: ").append(toIndentedString(credit)).append("\n");
    sb.append("    score: ").append(toIndentedString(score)).append("\n");
    sb.append("    isRestudy: ").append(toIndentedString(isRestudy)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

