package bjtu.student.portrait.model;

import bjtu.student.portrait.entity.HeadInfo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;

import java.net.URI;
import java.util.Objects;

/**
 * HeadInfo
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class HeadInfoRes {

    @JsonProperty("photoUrl")
    private JsonNullable<URI> photoUrl = JsonNullable.undefined();

    @JsonProperty("inSchool")
    private Boolean inSchool;

    @JsonProperty("dormWarning")
    private Boolean dormWarning;

    @JsonProperty("studyWarning")
    private Boolean studyWarning;

    @JsonProperty("archiveStatus")
    private ArchiveStatusEnum archiveStatus;

    @JsonProperty("mentor")
    private String mentor;

    @JsonProperty("teacher")
    private String teacher;

    public HeadInfoRes() {
    }

    public HeadInfoRes(HeadInfo headInfo) {
        this.photoUrl = JsonNullable.of(URI.create(headInfo.getPhotoUrl()));
        this.inSchool = Objects.equals(headInfo.getInSchool(), "1");
        this.dormWarning = Objects.equals(headInfo.getDormWarning(), "1");
        this.studyWarning = Objects.equals(headInfo.getStudyWarning(), "1");
        this.archiveStatus = ArchiveStatusEnum.valueOf(headInfo.getArchiveStatus());
        this.mentor = headInfo.getMentor();
        this.teacher = headInfo.getTeacher();
    }

    public HeadInfoRes photoUrl(URI photoUrl) {
        this.photoUrl = JsonNullable.of(photoUrl);
        return this;
    }

    /**
     * Get photoUrl
     *
     * @return photoUrl
     */
    @NotNull
    @Valid
    @Schema(name = "photoUrl", requiredMode = Schema.RequiredMode.REQUIRED)
    public JsonNullable<URI> getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(JsonNullable<URI> photoUrl) {
        this.photoUrl = photoUrl;
    }

    public HeadInfoRes inSchool(Boolean inSchool) {
        this.inSchool = inSchool;
        return this;
    }

    /**
     * Get inSchool
     *
     * @return inSchool
     */
    @NotNull
    @Schema(name = "inSchool", requiredMode = Schema.RequiredMode.REQUIRED)
    public Boolean getInSchool() {
        return inSchool;
    }

    public void setInSchool(Boolean inSchool) {
        this.inSchool = inSchool;
    }

    public HeadInfoRes inDorm(Boolean inDorm) {
        this.dormWarning = inDorm;
        return this;
    }

    /**
     * Get inDorm
     *
     * @return inDorm
     */
    @NotNull
    @Schema(name = "dormWarning", requiredMode = Schema.RequiredMode.REQUIRED)
    public Boolean getDormWarning() {
        return dormWarning;
    }

    public void setDormWarning(Boolean dormWarning) {
        this.dormWarning = dormWarning;
    }

    public HeadInfoRes studyWarning(Boolean studyWarning) {
        this.studyWarning = studyWarning;
        return this;
    }

    /**
     * Get inNormal
     *
     * @return inNormal
     */
    @NotNull
    @Schema(name = "studyWarning", requiredMode = Schema.RequiredMode.REQUIRED)
    public Boolean getStudyWarning() {
        return studyWarning;
    }

    public void setStudyWarning(Boolean studyWarning) {
        this.studyWarning = studyWarning;
    }

    public HeadInfoRes inArchive(ArchiveStatusEnum inArchive) {
        this.archiveStatus = inArchive;
        return this;
    }

    /**
     * Get inArchive
     *
     * @return inArchive
     */
    @NotNull
    @Schema(name = "archiveStatus", requiredMode = Schema.RequiredMode.REQUIRED)
    public HeadInfoRes.ArchiveStatusEnum getArchiveStatus() {
        return archiveStatus;
    }

    public void setArchiveStatus(ArchiveStatusEnum archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public HeadInfoRes mentor(String mentor) {
        this.mentor = mentor;
        return this;
    }

    /**
     * Get mentor
     *
     * @return mentor
     */
    @NotNull
    @Schema(name = "mentor", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getMentor() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor = mentor;
    }

    public HeadInfoRes teacher(String teacher) {
        this.teacher = teacher;
        return this;
    }

    /**
     * Get teacher
     *
     * @return teacher
     */
    @NotNull
    @Schema(name = "teacher", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeadInfoRes headInfoRes = (HeadInfoRes) o;
        return Objects.equals(this.photoUrl, headInfoRes.photoUrl) &&
                Objects.equals(this.inSchool, headInfoRes.inSchool) &&
                Objects.equals(this.dormWarning, headInfoRes.dormWarning) &&
                Objects.equals(this.studyWarning, headInfoRes.studyWarning) &&
                Objects.equals(this.archiveStatus, headInfoRes.archiveStatus) &&
                Objects.equals(this.mentor, headInfoRes.mentor) &&
                Objects.equals(this.teacher, headInfoRes.teacher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(photoUrl, inSchool, dormWarning, studyWarning, archiveStatus, mentor, teacher);
    }

    @Override
    public String toString() {
        String sb = "class HeadInfo {\n" +
                "    photoUrl: " + toIndentedString(photoUrl) + "\n" +
                "    inSchool: " + toIndentedString(inSchool) + "\n" +
                "    inDorm: " + toIndentedString(dormWarning) + "\n" +
                "    inNormal: " + toIndentedString(studyWarning) + "\n" +
                "    inArchive: " + toIndentedString(archiveStatus) + "\n" +
                "    mentor: " + toIndentedString(mentor) + "\n" +
                "    teacher: " + toIndentedString(teacher) + "\n" +
                "}";
        return sb;
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    /**
     * Gets or Sets inArchive
     */
    public enum ArchiveStatusEnum {
        NORMAL("正常"),

        STAY("保留学籍"),

        OUT("退学"),

        GRAD("毕业");

        private final String value;

        ArchiveStatusEnum(String value) {
            this.value = value;
        }

        @JsonCreator
        public static ArchiveStatusEnum fromValue(String value) {
            for (ArchiveStatusEnum b : ArchiveStatusEnum.values()) {
                if (b.value.equals(value)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + value + "'");
        }

        @JsonValue
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}

