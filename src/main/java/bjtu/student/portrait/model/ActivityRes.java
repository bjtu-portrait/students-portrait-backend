package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ActivityRes
 */
@Validated
@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2023-05-08T12:12:09.833102544Z[GMT]")


public class ActivityRes {
    @JsonProperty("semesterAmount")
    private Integer semesterAmount = null;

    @JsonProperty("semesterTally")
    private Integer semesterTally = null;

    @JsonProperty("activities")
    @Valid
    private List<Activity> activities = new ArrayList<>();

    public ActivityRes semesterAmount(Integer semesterAmount) {
        this.semesterAmount = semesterAmount;
        return this;
    }

    /**
     * Get semesterAmount
     *
     * @return semesterAmount
     **/
    @Schema(required = true, description = "")
    @NotNull

    public Integer getSemesterAmount() {
        return semesterAmount;
    }

    public void setSemesterAmount(Integer semesterAmount) {
        this.semesterAmount = semesterAmount;
    }

    public ActivityRes semesterTally(Integer semesterTally) {
        this.semesterTally = semesterTally;
        return this;
    }

    /**
     * Get semesterTally
     *
     * @return semesterTally
     **/
    @Schema(required = true, description = "")
    @NotNull

    public Integer getSemesterTally() {
        return semesterTally;
    }

    public void setSemesterTally(Integer semesterTally) {
        this.semesterTally = semesterTally;
    }

    public ActivityRes activities(List<Activity> activities) {
        this.activities = activities;
        return this;
    }

    public ActivityRes addActivitiesItem(Activity activitiesItem) {
        this.activities.add(activitiesItem);
        return this;
    }

    /**
     * Get activities
     *
     * @return activities
     **/
    @Schema(required = true, description = "")
    @NotNull
    @Valid
    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ActivityRes activityRes = (ActivityRes) o;
        return Objects.equals(this.semesterAmount, activityRes.semesterAmount) &&
                Objects.equals(this.semesterTally, activityRes.semesterTally) &&
                Objects.equals(this.activities, activityRes.activities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(semesterAmount, semesterTally, activities);
    }

    @Override
    public String toString() {

        String sb = "class ActivityRes {\n" +
                "    semesterAmount: " + toIndentedString(semesterAmount) + "\n" +
                "    semesterTally: " + toIndentedString(semesterTally) + "\n" +
                "    activities: " + toIndentedString(activities) + "\n" +
                "}";
        return sb;
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
