package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Counselling
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Counselling {

    @JsonProperty("date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate date;

    @JsonProperty("mentor")
    private String mentor;

    @JsonProperty("cls")
    private String cls;

    @JsonProperty("detail")
    private JsonNullable<String> detail = JsonNullable.undefined();

    public Counselling date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     */
    @NotNull
    @Valid
    @Schema(name = "date", requiredMode = Schema.RequiredMode.REQUIRED)
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Counselling mentor(String mentor) {
        this.mentor = mentor;
        return this;
    }

    /**
     * Get mentor
     *
     * @return mentor
     */
    @NotNull
    @Schema(name = "mentor", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getMentor() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor = mentor;
    }

    public Counselling cls(String cls) {
        this.cls = cls;
        return this;
    }

    /**
     * Get cls
     *
     * @return cls
     */
    @NotNull
    @Schema(name = "cls", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public Counselling detail(String detail) {
        this.detail = JsonNullable.of(detail);
        return this;
    }

    /**
     * Get detail
     *
     * @return detail
     */
    @NotNull
    @Schema(name = "detail", requiredMode = Schema.RequiredMode.REQUIRED)
    public JsonNullable<String> getDetail() {
        return detail;
    }

    public void setDetail(JsonNullable<String> detail) {
        this.detail = detail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Counselling counselling = (Counselling) o;
        return Objects.equals(this.date, counselling.date) &&
                Objects.equals(this.mentor, counselling.mentor) &&
                Objects.equals(this.cls, counselling.cls) &&
                Objects.equals(this.detail, counselling.detail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, mentor, detail);
    }

    @Override
    public String toString() {
        String sb = "class Counselling {\n" +
                "    date: " + toIndentedString(date) + "\n" +
                "    mentor: " + toIndentedString(mentor) + "\n" +
                "    cls: " + toIndentedString(cls) + "\n" +
                "    detail: " + toIndentedString(detail) + "\n" +
                "}";
        return sb;
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

