package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Scholarship
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Scholarship {

  @JsonProperty("name")
  private String name;

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate date;

  /**
   * Get name
   * @return name
  */
  @NotNull
  @Schema(name = "name", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getName() {
    return name;
  }

  @JsonProperty("level")
  private LevelEnum level;

  @JsonProperty("bonus")
  private Integer bonus;

  public Scholarship name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get date
   * @return date
  */
  @NotNull
  @Valid
  @Schema(name = "date", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getDate() {
    return date;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Scholarship date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Get level
   * @return level
  */
  @NotNull
  @Schema(name = "level", requiredMode = Schema.RequiredMode.REQUIRED)
  public LevelEnum getLevel() {
    return level;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Scholarship level(LevelEnum level) {
    this.level = level;
    return this;
  }

  /**
   * Get bonus
   * @return bonus
  */
  @NotNull
  @Schema(name = "bonus", requiredMode = Schema.RequiredMode.REQUIRED)
  public Integer getBonus() {
    return bonus;
  }

  public void setLevel(LevelEnum level) {
    this.level = level;
  }

  public Scholarship bonus(Integer bonus) {
    this.bonus = bonus;
    return this;
  }

  /**
   * Gets or Sets level
   */
  public enum LevelEnum {
    NATION("国家级"),

    PROVINCE("省市级"),

    SCHOOL("校级");

    private final String value;

    LevelEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static LevelEnum fromValue(String value) {
      for (LevelEnum b : LevelEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  public void setBonus(Integer bonus) {
    this.bonus = bonus;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Scholarship scholarship = (Scholarship) o;
    return Objects.equals(this.name, scholarship.name) &&
        Objects.equals(this.date, scholarship.date) &&
        Objects.equals(this.level, scholarship.level) &&
        Objects.equals(this.bonus, scholarship.bonus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, date, level, bonus);
  }

  @Override
  public String toString() {
    String sb = "class Scholarship {\n" +
            "    name: " + toIndentedString(name) + "\n" +
            "    date: " + toIndentedString(date) + "\n" +
            "    level: " + toIndentedString(level) + "\n" +
            "    bonus: " + toIndentedString(bonus) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

