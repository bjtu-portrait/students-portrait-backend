package bjtu.student.portrait.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * Average
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Average {

  @JsonProperty("semester")
  private String semester;

  @JsonProperty("self")
  private String self;

  @JsonProperty("other")
  private String other;

  public Average semester(String semester) {
    this.semester = semester;
    return this;
  }

  /**
   * Get semester
   * @return semester
  */
  @NotNull 
  @Schema(name = "semester", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getSemester() {
    return semester;
  }

  public void setSemester(String semester) {
    this.semester = semester;
  }

  public Average self(String self) {
    this.self = self;
    return this;
  }

  /**
   * Get self
   * @return self
  */
  @NotNull 
  @Schema(name = "self", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getSelf() {
    return self;
  }

  public void setSelf(String self) {
    this.self = self;
  }

  public Average other(String other) {
    this.other = other;
    return this;
  }

  /**
   * Get other
   * @return other
  */
  @NotNull 
  @Schema(name = "other", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getOther() {
    return other;
  }

  public void setOther(String other) {
    this.other = other;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Average average = (Average) o;
    return Objects.equals(this.semester, average.semester) &&
        Objects.equals(this.self, average.self) &&
        Objects.equals(this.other, average.other);
  }

  @Override
  public int hashCode() {
    return Objects.hash(semester, self, other);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Average {\n");
    sb.append("    semester: ").append(toIndentedString(semester)).append("\n");
    sb.append("    self: ").append(toIndentedString(self)).append("\n");
    sb.append("    other: ").append(toIndentedString(other)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

