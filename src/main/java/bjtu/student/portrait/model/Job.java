package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Job
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Job {

  @JsonProperty("amount")
  private Integer amount;

  @JsonProperty("jobTimes")
  @Valid
  private List<JobTime> jobTimes = new ArrayList<>();

  public Job amount(Integer amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   *
   * @return amount
   */
  @NotNull
  @Schema(name = "amount", requiredMode = Schema.RequiredMode.REQUIRED)
  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Job jobTimes(List<JobTime> jobTimes) {
    this.jobTimes = jobTimes;
    return this;
  }

  public Job addJobsItem(JobTime jobTimesItem) {
    this.jobTimes.add(jobTimesItem);
    return this;
  }

  /**
   * Get jobTimes
   *
   * @return jobTimes
   */
  @NotNull
  @Valid
  @Schema(name = "jobTimes", requiredMode = Schema.RequiredMode.REQUIRED)
  public List<JobTime> getJobTimes() {
    return jobTimes;
  }

  public void setJobTimes(List<JobTime> jobTimes) {
    this.jobTimes = jobTimes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Job job = (Job) o;
    return Objects.equals(this.amount, job.amount) &&
            Objects.equals(this.jobTimes, job.jobTimes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, jobTimes);
  }

  @Override
  public String toString() {
    String sb = "class Job {\n" +
            "    amount: " + toIndentedString(amount) + "\n" +
            "    jobTimes: " + toIndentedString(jobTimes) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

