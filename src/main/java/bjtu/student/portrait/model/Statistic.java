package bjtu.student.portrait.model;

import java.net.URI;
import java.util.Objects;
import bjtu.student.portrait.model.Average;
import bjtu.student.portrait.model.BackTime;
import bjtu.student.portrait.model.Record;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * Statistic
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Statistic {

  @JsonProperty("average")
  private String average;

  @JsonProperty("times")
  @Valid
  private List<BackTime> times = new ArrayList<>();

  @JsonProperty("averages")
  @Valid
  private List<Average> averages = new ArrayList<>();

  @JsonProperty("records")
  @Valid
  private List<Record> records = new ArrayList<>();

  public Statistic average(String average) {
    this.average = average;
    return this;
  }

  /**
   * Get average
   * @return average
  */
  @NotNull 
  @Schema(name = "average", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getAverage() {
    return average;
  }

  public void setAverage(String average) {
    this.average = average;
  }

  public Statistic times(List<BackTime> times) {
    this.times = times;
    return this;
  }

  public Statistic addTimesItem(BackTime timesItem) {
    this.times.add(timesItem);
    return this;
  }

  /**
   * Get times
   * @return times
  */
  @NotNull @Valid 
  @Schema(name = "times", requiredMode = Schema.RequiredMode.REQUIRED)
  public List<BackTime> getTimes() {
    return times;
  }

  public void setTimes(List<BackTime> times) {
    this.times = times;
  }

  public Statistic averages(List<Average> averages) {
    this.averages = averages;
    return this;
  }

  public Statistic addAveragesItem(Average averagesItem) {
    this.averages.add(averagesItem);
    return this;
  }

  /**
   * Get averages
   * @return averages
  */
  @NotNull @Valid 
  @Schema(name = "averages", requiredMode = Schema.RequiredMode.REQUIRED)
  public List<Average> getAverages() {
    return averages;
  }

  public void setAverages(List<Average> averages) {
    this.averages = averages;
  }

  public Statistic records(List<Record> records) {
    this.records = records;
    return this;
  }

  public Statistic addRecordsItem(Record recordsItem) {
    this.records.add(recordsItem);
    return this;
  }

  /**
   * Get records
   * @return records
  */
  @NotNull @Valid 
  @Schema(name = "records", requiredMode = Schema.RequiredMode.REQUIRED)
  public List<Record> getRecords() {
    return records;
  }

  public void setRecords(List<Record> records) {
    this.records = records;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Statistic statistic = (Statistic) o;
    return Objects.equals(this.average, statistic.average) &&
        Objects.equals(this.times, statistic.times) &&
        Objects.equals(this.averages, statistic.averages) &&
        Objects.equals(this.records, statistic.records);
  }

  @Override
  public int hashCode() {
    return Objects.hash(average, times, averages, records);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Statistic {\n");
    sb.append("    average: ").append(toIndentedString(average)).append("\n");
    sb.append("    times: ").append(toIndentedString(times)).append("\n");
    sb.append("    averages: ").append(toIndentedString(averages)).append("\n");
    sb.append("    records: ").append(toIndentedString(records)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

