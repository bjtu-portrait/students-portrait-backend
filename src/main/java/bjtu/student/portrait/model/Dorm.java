package bjtu.student.portrait.model;

import java.net.URI;
import java.util.Objects;
import bjtu.student.portrait.model.Appraisal;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * Dorm
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Dorm {

  @JsonProperty("building")
  private String building;

  @JsonProperty("room")
  private String room;

  @JsonProperty("appraisals")
  @Valid
  private List<Appraisal> appraisals = new ArrayList<>();

  public Dorm building(String building) {
    this.building = building;
    return this;
  }

  /**
   * Get building
   * @return building
  */
  @NotNull 
  @Schema(name = "building", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getBuilding() {
    return building;
  }

  public void setBuilding(String building) {
    this.building = building;
  }

  public Dorm room(String room) {
    this.room = room;
    return this;
  }

  /**
   * Get room
   * @return room
  */
  @NotNull 
  @Schema(name = "room", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getRoom() {
    return room;
  }

  public void setRoom(String room) {
    this.room = room;
  }

  public Dorm appraisals(List<Appraisal> appraisals) {
    this.appraisals = appraisals;
    return this;
  }

  public Dorm addAppraisalsItem(Appraisal appraisalsItem) {
    this.appraisals.add(appraisalsItem);
    return this;
  }

  /**
   * Get appraisals
   * @return appraisals
  */
  @NotNull @Valid 
  @Schema(name = "appraisals", requiredMode = Schema.RequiredMode.REQUIRED)
  public List<Appraisal> getAppraisals() {
    return appraisals;
  }

  public void setAppraisals(List<Appraisal> appraisals) {
    this.appraisals = appraisals;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dorm dorm = (Dorm) o;
    return Objects.equals(this.building, dorm.building) &&
        Objects.equals(this.room, dorm.room) &&
        Objects.equals(this.appraisals, dorm.appraisals);
  }

  @Override
  public int hashCode() {
    return Objects.hash(building, room, appraisals);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Dorm {\n");
    sb.append("    building: ").append(toIndentedString(building)).append("\n");
    sb.append("    room: ").append(toIndentedString(room)).append("\n");
    sb.append("    appraisals: ").append(toIndentedString(appraisals)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

