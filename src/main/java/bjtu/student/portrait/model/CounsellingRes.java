package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * CounsellingRes
 */
@Validated
@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2023-05-08T12:12:09.833102544Z[GMT]")


public class CounsellingRes {
    @JsonProperty("semesterAmount")
    private Integer semesterAmount = null;

    @JsonProperty("counsellings")
    @Valid
    private List<Counselling> counsellings = new ArrayList<>();

    public CounsellingRes semesterAmount(Integer semesterAmount) {
        this.semesterAmount = semesterAmount;
        return this;
    }

    /**
     * Get semesterAmount
     *
     * @return semesterAmount
     **/
    @Schema(required = true, description = "")
    @NotNull

    public Integer getSemesterAmount() {
        return semesterAmount;
    }

    public void setSemesterAmount(Integer semesterAmount) {
        this.semesterAmount = semesterAmount;
    }

    public CounsellingRes counsellings(List<Counselling> counsellings) {
        this.counsellings = counsellings;
        return this;
    }

    public CounsellingRes addCounsellingsItem(Counselling counsellingsItem) {
        this.counsellings.add(counsellingsItem);
        return this;
    }

    /**
     * Get counsellings
     *
     * @return counsellings
     **/
    @Schema(required = true, description = "")
    @NotNull
    @Valid
    public List<Counselling> getCounsellings() {
        return counsellings;
    }

    public void setCounsellings(List<Counselling> counsellings) {
        this.counsellings = counsellings;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CounsellingRes counsellingRes = (CounsellingRes) o;
        return Objects.equals(this.semesterAmount, counsellingRes.semesterAmount) &&
                Objects.equals(this.counsellings, counsellingRes.counsellings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(semesterAmount, counsellings);
    }

    @Override
    public String toString() {

        String sb = "class CounsellingRes {\n" +
                "    semesterAmount: " + toIndentedString(semesterAmount) + "\n" +
                "    counsellings: " + toIndentedString(counsellings) + "\n" +
                "}";
        return sb;
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
