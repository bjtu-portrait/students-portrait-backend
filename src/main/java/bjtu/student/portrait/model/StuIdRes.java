package bjtu.student.portrait.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * StuIdRes
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class StuIdRes {

  /**
   * 1成功返回学号，调用个人信息api；2重名，调用高级搜索姓名搜索api
   */
  public enum TypeEnum {
    NUMBER_1(1),
    
    NUMBER_2(2),
    
    NUMBER_3(3);

    private Integer value;

    TypeEnum(Integer value) {
      this.value = value;
    }

    @JsonValue
    public Integer getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(Integer value) {
      for (TypeEnum b : TypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("type")
  private TypeEnum type;

  @JsonProperty("stuId")
  private String stuId;

  public StuIdRes type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * 1成功返回学号，调用个人信息api；2重名，调用高级搜索姓名搜索api
   * @return type
  */
  @NotNull 
  @Schema(name = "type", description = "1成功返回学号，调用个人信息api；2重名，调用高级搜索姓名搜索api", requiredMode = Schema.RequiredMode.REQUIRED)
  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public StuIdRes stuId(String stuId) {
    this.stuId = stuId;
    return this;
  }

  /**
   * Get stuId
   * @return stuId
  */
  
  @Schema(name = "stuId", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
  public String getStuId() {
    return stuId;
  }

  public void setStuId(String stuId) {
    this.stuId = stuId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StuIdRes stuIdRes = (StuIdRes) o;
    return Objects.equals(this.type, stuIdRes.type) &&
        Objects.equals(this.stuId, stuIdRes.stuId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, stuId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StuIdRes {\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    stuId: ").append(toIndentedString(stuId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

