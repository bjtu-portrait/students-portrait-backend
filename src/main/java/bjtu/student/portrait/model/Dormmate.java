package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;

import java.net.URI;
import java.util.Objects;

/**
 * Dormmate
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Dormmate {

    @JsonProperty("name")
    private String name;

    @JsonProperty("photoUrl")
    private JsonNullable<URI> photoUrl = JsonNullable.undefined();

    @JsonProperty("cls")
    private String cls;

    @JsonProperty("ancestry")
    private String ancestry;

    @JsonProperty("stuId")
    private String stuId;

    public Dormmate name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     */
    @NotNull
    @Schema(name = "name", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dormmate photoUrl(URI photoUrl) {
        this.photoUrl = JsonNullable.of(photoUrl);
        return this;
    }

    /**
     * Get photoUrl
     *
     * @return photoUrl
     */
    @NotNull
    @Schema(name = "photoUrl", requiredMode = Schema.RequiredMode.REQUIRED)
    public JsonNullable<URI> getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(JsonNullable<URI> photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Dormmate cls(String cls) {
        this.cls = cls;
        return this;
    }

    /**
     * Get cls
     *
     * @return cls
     */
    @NotNull
    @Schema(name = "cls", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public Dormmate ancestry(String ancestry) {
        this.ancestry = ancestry;
        return this;
    }

    /**
     * Get ancestry
     *
     * @return ancestry
     */
    @NotNull
    @Schema(name = "ancestry", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getAncestry() {
        return ancestry;
    }

    public void setAncestry(String ancestry) {
        this.ancestry = ancestry;
    }

    public Dormmate stuId(String stuId) {
        this.stuId = stuId;
        return this;
    }

    /**
     * Get stuId
     *
     * @return stuId
     */
    @NotNull
    @Schema(name = "stuId", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dormmate dormmate = (Dormmate) o;
        return Objects.equals(this.name, dormmate.name) &&
                Objects.equals(this.photoUrl, dormmate.photoUrl) &&
                Objects.equals(this.cls, dormmate.cls) &&
                Objects.equals(this.ancestry, dormmate.ancestry) &&
                Objects.equals(this.stuId, dormmate.stuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cls, ancestry, stuId);
    }

    @Override
    public String toString() {
        String sb = "class Dormmate {\n" +
                "    name: " + toIndentedString(name) + "\n" +
                "    photoUrl: " + toIndentedString(photoUrl) + "\n" +
                "    cls: " + toIndentedString(cls) + "\n" +
                "    ancestry: " + toIndentedString(ancestry) + "\n" +
                "    stuId: " + toIndentedString(stuId) + "\n" +
                "}";
        return sb;
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

