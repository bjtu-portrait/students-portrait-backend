package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * LoanRes
 */
@Validated
@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2023-05-11T14:57:49.984782325Z[GMT]")


public class LoanRes {
  @JsonProperty("repayAmount")
  private Integer repayAmount = null;

  @JsonProperty("loans")
  @Valid
  private List<Loan> loans = new ArrayList<>();

  public LoanRes repayAmount(Integer repayAmount) {
    this.repayAmount = repayAmount;
    return this;
  }

  /**
   * Get repayAmount
   *
   * @return repayAmount
   **/
  @Schema(required = true, description = "")
  @NotNull

  public Integer getRepayAmount() {
    return repayAmount;
  }

  public void setRepayAmount(Integer repayAmount) {
    this.repayAmount = repayAmount;
  }

  public LoanRes loans(List<Loan> loans) {
    this.loans = loans;
    return this;
  }

  public LoanRes addLoansItem(Loan loansItem) {
    this.loans.add(loansItem);
    return this;
  }

  /**
   * Get loans
   *
   * @return loans
   **/
  @Schema(required = true, description = "")
  @NotNull
  @Valid
  public List<Loan> getLoans() {
    return loans;
  }

  public void setLoans(List<Loan> loans) {
    this.loans = loans;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LoanRes loanRes = (LoanRes) o;
    return Objects.equals(this.repayAmount, loanRes.repayAmount) &&
            Objects.equals(this.loans, loanRes.loans);
  }

  @Override
  public int hashCode() {
    return Objects.hash(repayAmount, loans);
  }

  @Override
  public String toString() {

    String sb = "class LoanRes {\n" +
            "    repayAmount: " + toIndentedString(repayAmount) + "\n" +
            "    loans: " + toIndentedString(loans) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
