package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * StudentInfo
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class StudentInfoRes {

    @JsonProperty("grade")
    private GradeEnum grade;
    @JsonProperty("school")
    private String school;
    @JsonProperty("major")
    private String major;
    @JsonProperty("cls")
    private String cls;
    @JsonProperty("source")
    private SourceEnum source;
    @JsonProperty("span")
    private Float span;
    @JsonProperty("entry_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate entryDate;
    @JsonProperty("grad_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private JsonNullable<LocalDate> gradDate = JsonNullable.undefined();
    @JsonProperty("stu_id")
    private String stuId;

    public StudentInfoRes grade(GradeEnum grade) {
        this.grade = grade;
        return this;
    }

    /**
     * Get grade
     *
     * @return grade
     */
    @NotNull
    @Schema(name = "grade", requiredMode = Schema.RequiredMode.REQUIRED)
    public GradeEnum getGrade() {
        return grade;
    }

    public void setGrade(GradeEnum grade) {
        this.grade = grade;
    }

    public StudentInfoRes school(String school) {
        this.school = school;
        return this;
    }

    /**
     * Get school
     *
     * @return school
     */
    @NotNull
    @Schema(name = "school", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public StudentInfoRes major(String major) {
        this.major = major;
        return this;
    }

    /**
     * Get major
     *
     * @return major
     */
    @NotNull
    @Schema(name = "major", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public StudentInfoRes cls(String cls) {
        this.cls = cls;
        return this;
    }

    /**
     * Get cls
     *
     * @return cls
     */
    @NotNull
    @Schema(name = "cls", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public StudentInfoRes source(SourceEnum source) {
        this.source = source;
        return this;
    }

    /**
     * Get source
     *
     * @return source
     */
    @NotNull
    @Schema(name = "source", requiredMode = Schema.RequiredMode.REQUIRED)
    public SourceEnum getSource() {
        return source;
    }

    public void setSource(SourceEnum source) {
        this.source = source;
    }

    public StudentInfoRes span(Float span) {
        this.span = span;
        return this;
    }

    /**
     * Get span
     *
     * @return span
     */
    @NotNull
    @Schema(name = "span", requiredMode = Schema.RequiredMode.REQUIRED)
    public Float getSpan() {
        return span;
    }

    public void setSpan(Float span) {
        this.span = span;
    }

    public StudentInfoRes entryDate(LocalDate entryDate) {
        this.entryDate = entryDate;
        return this;
    }

    /**
     * Get entryDate
     *
     * @return entryDate
     */
    @NotNull
    @Valid
    @Schema(name = "entry_date", requiredMode = Schema.RequiredMode.REQUIRED)
    public LocalDate getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDate entryDate) {
        this.entryDate = entryDate;
    }

    public StudentInfoRes gradDate(LocalDate gradDate) {
        this.gradDate = JsonNullable.of(gradDate);
        return this;
    }

    /**
     * Get gradDate
     *
     * @return gradDate
     */
    @NotNull
    @Valid
    @Schema(name = "grad_date", requiredMode = Schema.RequiredMode.REQUIRED)
    public JsonNullable<LocalDate> getGradDate() {
        return gradDate;
    }

    public void setGradDate(JsonNullable<LocalDate> gradDate) {
        this.gradDate = gradDate;
    }

    public StudentInfoRes stuId(String stuId) {
        this.stuId = stuId;
        return this;
    }

    /**
     * Get stuId
     *
     * @return stuId
     */
    @NotNull
    @Schema(name = "stu_id", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StudentInfoRes studentInfoRes = (StudentInfoRes) o;
        return Objects.equals(this.grade, studentInfoRes.grade) &&
                Objects.equals(this.school, studentInfoRes.school) &&
                Objects.equals(this.major, studentInfoRes.major) &&
                Objects.equals(this.cls, studentInfoRes.cls) &&
                Objects.equals(this.source, studentInfoRes.source) &&
                Objects.equals(this.span, studentInfoRes.span) &&
                Objects.equals(this.entryDate, studentInfoRes.entryDate) &&
                Objects.equals(this.gradDate, studentInfoRes.gradDate) &&
                Objects.equals(this.stuId, studentInfoRes.stuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(grade, school, major, cls, source, span, entryDate, gradDate, stuId);
    }

    @Override
    public String toString() {
        String sb = "class StudentInfo {\n" +
                "    grade: " + toIndentedString(grade) + "\n" +
                "    school: " + toIndentedString(school) + "\n" +
                "    major: " + toIndentedString(major) + "\n" +
                "    cls: " + toIndentedString(cls) + "\n" +
                "    source: " + toIndentedString(source) + "\n" +
                "    span: " + toIndentedString(span) + "\n" +
                "    entryDate: " + toIndentedString(entryDate) + "\n" +
                "    gradDate: " + toIndentedString(gradDate) + "\n" +
                "    stuId: " + toIndentedString(stuId) + "\n" +
                "}";
        return sb;
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    /**
     * Gets or Sets grade
     */
    public enum GradeEnum {
        BACHELOR("本科"),

        SECOND_BACHELOR("二学位"),

        MASTER("硕士"),

        DOCTOR("博士");

        private final String value;

        GradeEnum(String value) {
            this.value = value;
        }

        @JsonCreator
        public static GradeEnum fromValue(String value) {
            for (GradeEnum b : GradeEnum.values()) {
                if (b.value.equals(value)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + value + "'");
        }

        @JsonValue
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }

    /**
     * Gets or Sets source
     */
    public enum SourceEnum {
        NON_TARGETED("非定向"),

        TARGETED("定向"),

        FOREIGN("留学生"),

        IN_JOB("在职");

        private final String value;

        SourceEnum(String value) {
            this.value = value;
        }

        @JsonCreator
        public static SourceEnum fromValue(String value) {
            for (SourceEnum b : SourceEnum.values()) {
                if (b.value.equals(value)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + value + "'");
        }

        @JsonValue
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}

