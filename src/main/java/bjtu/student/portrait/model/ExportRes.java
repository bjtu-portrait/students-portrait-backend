package bjtu.student.portrait.model;

import bjtu.student.portrait.converter.*;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import org.openapitools.jackson.nullable.JsonNullable;

import java.net.URI;
import java.time.LocalDate;
import java.util.Objects;

@ContentRowHeight(25)
public class ExportRes {
    @ExcelProperty("姓名")
    private String name;

    @ExcelProperty(value = "性别", converter = GenderEnumConverter.class)
    private BaseInfoRes.GenderEnum gender;

    @ExcelProperty("出生日期")
    @ColumnWidth(12)
    private LocalDate birthday;

    @ExcelProperty("民族")
    private String nation;

    @ExcelProperty("籍贯")
    private String ancestry;

    @ExcelProperty("家庭住址")
    @ColumnWidth(25)
    private String address;

    @ExcelProperty(value = "婚姻状况", converter = MarriedBooleanConverter.class)
    private Boolean married;

    @ExcelProperty("联系电话")
    @ColumnWidth(15)
    private String phone;

    @ExcelProperty(value = "政治面貌", converter = PoliticalEnumConverter.class)
    private BaseInfoRes.PoliticalEnum political;

    @ExcelProperty(value = "党支部", converter = NullableStringConverter.class)
    @ColumnWidth(15)
    private JsonNullable<String> org = JsonNullable.undefined();

    @ExcelProperty("学号")
    @ColumnWidth(10)
    private String stuId;

    @ExcelProperty(value = "照片", converter = NullableURIImageConverter.class)
    @ColumnWidth(5)
    @ExcelIgnore
    private JsonNullable<URI> photoUrl = JsonNullable.undefined();

    @ExcelProperty(value = "是否在校", converter = BooleanStringConverter.class)
    private Boolean inSchool;

    @ExcelProperty(value = "归寝预警", converter = BooleanStringConverter.class)
    private Boolean dormWarning;

    @ExcelProperty(value = "学业预警", converter = BooleanStringConverter.class)
    private Boolean studyWarning;

    @ExcelProperty(value = "档案状态", converter = ArchiveStatusEnumConverter.class)
    private HeadInfoRes.ArchiveStatusEnum archiveStatus;

    @ExcelProperty("辅导员")
    private String mentor;

    @ExcelProperty("班主任")
    private String teacher;

    @ExcelProperty("学院")
    @ColumnWidth(20)
    private String school;

    @ExcelProperty("专业")
    @ColumnWidth(20)
    private String major;

    @ExcelProperty("班级")
    private String cls;

    @ExcelProperty(value = "学历类型", converter = GradeEnumStringConverter.class)
    private StudentInfoRes.GradeEnum grade;

    @ExcelProperty(value = "生源类型", converter = SourceEnumStringConverter.class)
    private StudentInfoRes.SourceEnum source;

    @ExcelProperty("学制")
    private Float span;

    @ExcelProperty("入学时间")
    @ColumnWidth(12)
    private LocalDate entryDate;

    @ExcelProperty(value = "毕业时间", converter = JsonNullableLocalDateConverter.class)
    @ColumnWidth(12)
    private JsonNullable<LocalDate> gradDate = JsonNullable.undefined();


    public StudentInfoRes.GradeEnum getGrade() {
        return grade;
    }

    public void setGrade(StudentInfoRes.GradeEnum grade) {
        this.grade = grade;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public StudentInfoRes.SourceEnum getSource() {
        return source;
    }

    public void setSource(StudentInfoRes.SourceEnum source) {
        this.source = source;
    }

    public Float getSpan() {
        return span;
    }

    public void setSpan(Float span) {
        this.span = span;
    }

    public LocalDate getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDate entryDate) {
        this.entryDate = entryDate;
    }

    public JsonNullable<LocalDate> getGradDate() {
        return gradDate;
    }

    public void setGradDate(JsonNullable<LocalDate> gradDate) {
        this.gradDate = gradDate;
    }

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    public JsonNullable<URI> getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(JsonNullable<URI> photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Boolean getInSchool() {
        return inSchool;
    }

    public void setInSchool(Boolean inSchool) {
        this.inSchool = inSchool;
    }

    public Boolean getDormWarning() {
        return dormWarning;
    }

    public void setDormWarning(Boolean dormWarning) {
        this.dormWarning = dormWarning;
    }

    public Boolean getStudyWarning() {
        return studyWarning;
    }

    public void setStudyWarning(Boolean studyWarning) {
        this.studyWarning = studyWarning;
    }

    public HeadInfoRes.ArchiveStatusEnum getArchiveStatus() {
        return archiveStatus;
    }

    public void setArchiveStatus(HeadInfoRes.ArchiveStatusEnum archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public String getMentor() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor = mentor;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseInfoRes.GenderEnum getGender() {
        return gender;
    }

    public void setGender(BaseInfoRes.GenderEnum gender) {
        this.gender = gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getAncestry() {
        return ancestry;
    }

    public void setAncestry(String ancestry) {
        this.ancestry = ancestry;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getMarried() {
        return married;
    }

    public void setMarried(Boolean married) {
        this.married = married;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BaseInfoRes.PoliticalEnum getPolitical() {
        return political;
    }

    public void setPolitical(BaseInfoRes.PoliticalEnum political) {
        this.political = political;
    }

    public JsonNullable<String> getOrg() {
        return org;
    }

    public void setOrg(JsonNullable<String> org) {
        this.org = org;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExportRes exportRes = (ExportRes) o;
        return grade == exportRes.grade && Objects.equals(school, exportRes.school) && Objects.equals(major, exportRes.major) && Objects.equals(cls, exportRes.cls) && source == exportRes.source && Objects.equals(span, exportRes.span) && Objects.equals(entryDate, exportRes.entryDate) && Objects.equals(gradDate, exportRes.gradDate) && Objects.equals(stuId, exportRes.stuId) && Objects.equals(photoUrl, exportRes.photoUrl) && Objects.equals(inSchool, exportRes.inSchool) && Objects.equals(dormWarning, exportRes.dormWarning) && Objects.equals(studyWarning, exportRes.studyWarning) && archiveStatus == exportRes.archiveStatus && Objects.equals(mentor, exportRes.mentor) && Objects.equals(teacher, exportRes.teacher) && Objects.equals(name, exportRes.name) && gender == exportRes.gender && Objects.equals(birthday, exportRes.birthday) && Objects.equals(nation, exportRes.nation) && Objects.equals(ancestry, exportRes.ancestry) && Objects.equals(address, exportRes.address) && Objects.equals(married, exportRes.married) && Objects.equals(phone, exportRes.phone) && political == exportRes.political && Objects.equals(org, exportRes.org);
    }

    @Override
    public int hashCode() {
        return Objects.hash(grade, school, major, cls, source, span, entryDate, gradDate, stuId, photoUrl, inSchool, dormWarning, studyWarning, archiveStatus, mentor, teacher, name, gender, birthday, nation, ancestry, address, married, phone, political, org);
    }
}
