package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.constraints.NotNull;

import java.util.Objects;

/**
 * Student
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Student {

    @JsonProperty("name")
    private String name;

    @JsonProperty("stuId")
    private String stuId;

    @JsonProperty("major")
    private String major;

    @JsonProperty("cls")
    private String cls;

    @JsonProperty("grade")
    private GradeEnum grade;

    /**
     * 姓名
     *
     * @return name
     */
    @NotNull
  @Schema(name = "name", description = "姓名", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Student name(String name) {
    this.name = name;
    return this;
  }

  /**
   * 学号
   *
   * @return stuId
   */
  @NotNull
  @Schema(name = "stuId", description = "学号", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getStuId() {
    return stuId;
  }

  public void setStuId(String stuId) {
    this.stuId = stuId;
  }

  public Student stuId(String stuId) {
    this.stuId = stuId;
    return this;
  }

  /**
   * 专业
   *
   * @return major
   */
  @NotNull
  @Schema(name = "major", description = "专业", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getMajor() {
    return major;
  }

  public void setMajor(String major) {
    this.major = major;
  }

  public Student major(String major) {
    this.major = major;
    return this;
  }

  public Student propertyClass(String propertyClass) {
      this.cls = propertyClass;
    return this;
  }

    /**
     * 班级
     *
     * @return propertyClass
     */
    @NotNull
    @Schema(name = "class", description = "班级", requiredMode = Schema.RequiredMode.REQUIRED)
    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * 类型
     *
     * @return grade
     */
    @NotNull
    @Schema(name = "grade", description = "类型", requiredMode = Schema.RequiredMode.REQUIRED)
    public GradeEnum getGrade() {
    return grade;
  }

  public void setGrade(GradeEnum grade) {
    this.grade = grade;
  }

  public Student grade(GradeEnum grade) {
    this.grade = grade;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Student student = (Student) o;
      return Objects.equals(this.name, student.name) &&
              Objects.equals(this.stuId, student.stuId) &&
              Objects.equals(this.major, student.major) &&
              Objects.equals(this.cls, student.cls) &&
              Objects.equals(this.grade, student.grade);
  }

  @Override
  public int hashCode() {
      return Objects.hash(name, stuId, major, cls, grade);
  }

  @Override
  public String toString() {
      String sb = "class Student {\n" +
              "    name: " + toIndentedString(name) + "\n" +
              "    stuId: " + toIndentedString(stuId) + "\n" +
              "    major: " + toIndentedString(major) + "\n" +
              "    cls: " + toIndentedString(cls) + "\n" +
              "    grade: " + toIndentedString(grade) + "\n" +
            "}";
      return sb;
  }

    /**
     * 类型
     */
    public enum GradeEnum {
        BACHELOR("1", "本科"),

        SECOND_BACHELOR("2", "二学位"),

        MASTER("3", "硕士"),

        DOCTOR("4", "博士");

        private final String code;
        private final String value;

        GradeEnum(String code, String value) {
            this.code = code;
            this.value = value;
    }

    @JsonCreator
    public static GradeEnum fromValue(String value) {
      for (GradeEnum b : GradeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

    @JsonCreator
    public static GradeEnum fromCode(String code) {
      for (GradeEnum b : GradeEnum.values()) {
        if (b.code.equals(code)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected code '" + code + "'");
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

