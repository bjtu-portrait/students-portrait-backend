package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Punishment
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Punishment {

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate date;

  /**
   * Get date
   * @return date
  */
  @NotNull
  @Valid
  @Schema(name = "date", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getDate() {
    return date;
  }

  @JsonProperty("type")
  private TypeEnum type;

  @JsonProperty("reason")
  private JsonNullable<String> reason = JsonNullable.undefined();

  public Punishment date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Get type
   * @return type
  */
  @NotNull
  @Schema(name = "type", requiredMode = Schema.RequiredMode.REQUIRED)
  public TypeEnum getType() {
    return type;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Punishment type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * Get reason
   * @return reason
  */
  @NotNull
  @Schema(name = "reason", requiredMode = Schema.RequiredMode.REQUIRED)
  public JsonNullable<String> getReason() {
    return reason;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Punishment reason(String reason) {
    this.reason = JsonNullable.of(reason);
    return this;
  }

  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
      WARNING("警告"),

      ERROR("记过"),

      SEVERE_WARNING("严重警告"),

      PROBATION("留校察看");

      private final String value;

      TypeEnum(String value) {
          this.value = value;
      }

      @JsonValue
      public String getValue() {
          return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String value) {
      for (TypeEnum b : TypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  public void setReason(JsonNullable<String> reason) {
    this.reason = reason;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Punishment punishment = (Punishment) o;
    return Objects.equals(this.date, punishment.date) &&
        Objects.equals(this.type, punishment.type) &&
        Objects.equals(this.reason, punishment.reason);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, type, reason);
  }

  @Override
  public String toString() {
    String sb = "class Punishment {\n" +
            "    date: " + toIndentedString(date) + "\n" +
            "    type: " + toIndentedString(type) + "\n" +
            "    reason: " + toIndentedString(reason) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

