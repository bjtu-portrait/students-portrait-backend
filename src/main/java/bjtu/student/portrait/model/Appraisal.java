package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Appraisal
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Appraisal {

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate date;

  /**
   * Get date
   * @return date
  */
  @NotNull
  @Valid
  @Schema(name = "date", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getDate() {
    return date;
  }

  @JsonProperty("type")
  private TypeEnum type;

  @JsonProperty("detail")
  private JsonNullable<String> detail = JsonNullable.undefined();

  public Appraisal date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Get type
   * @return type
  */
  @NotNull
  @Schema(name = "type", requiredMode = Schema.RequiredMode.REQUIRED)
  public TypeEnum getType() {
    return type;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Appraisal type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * Get detail
   * @return detail
  */
  @NotNull
  @Schema(name = "detail", requiredMode = Schema.RequiredMode.REQUIRED)
  public JsonNullable<String> getDetail() {
    return detail;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Appraisal detail(String detail) {
    this.detail = JsonNullable.of(detail);
    return this;
  }

  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
    REWARD("获奖"),

    WARNING("警告");

    private final String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String value) {
      for (TypeEnum b : TypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  public void setDetail(JsonNullable<String> detail) {
    this.detail = detail;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Appraisal appraisal = (Appraisal) o;
    return Objects.equals(this.date, appraisal.date) &&
        Objects.equals(this.type, appraisal.type) &&
        Objects.equals(this.detail, appraisal.detail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, type, detail);
  }

  @Override
  public String toString() {
    String sb = "class Appraisal {\n" +
            "    date: " + toIndentedString(date) + "\n" +
            "    type: " + toIndentedString(type) + "\n" +
            "    detail: " + toIndentedString(detail) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

