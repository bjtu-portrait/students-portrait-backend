package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * BaseInfo
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class BaseInfoRes {

  @JsonProperty("name")
  private String name;

  /**
   * Get name
   * @return name
  */
  @NotNull
  @Schema(name = "name", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getName() {
    return name;
  }

  @JsonProperty("gender")
  private GenderEnum gender;

  @JsonProperty("birthday")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate birthday;

  @JsonProperty("id")
  private String id;

  @JsonProperty("nation")
  private String nation;

  @JsonProperty("ancestry")
  private String ancestry;

  @JsonProperty("address")
  private String address;

  @JsonProperty("married")
  private Boolean married;

  @JsonProperty("phone")
  private String phone;

  /**
   * Get gender
   * @return gender
  */
  @NotNull
  @Schema(name = "gender", requiredMode = Schema.RequiredMode.REQUIRED)
  public GenderEnum getGender() {
    return gender;
  }

  @JsonProperty("political")
  private PoliticalEnum political;

  @JsonProperty("org")
  private JsonNullable<String> org = JsonNullable.undefined();

  public BaseInfoRes name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get birthday
   * @return birthday
  */
  @NotNull
  @Valid
  @Schema(name = "birthday", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getBirthday() {
    return birthday;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BaseInfoRes gender(GenderEnum gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @NotNull
  @Schema(name = "id", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getId() {
    return id;
  }

  public void setGender(GenderEnum gender) {
    this.gender = gender;
  }

  public BaseInfoRes birthday(LocalDate birthday) {
    this.birthday = birthday;
    return this;
  }

  /**
   * Get nation
   * @return nation
  */
  @NotNull
  @Schema(name = "nation", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getNation() {
    return nation;
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
  }

  public BaseInfoRes id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get ancestry
   * @return ancestry
  */
  @NotNull
  @Schema(name = "ancestry", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getAncestry() {
    return ancestry;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BaseInfoRes nation(String nation) {
    this.nation = nation;
    return this;
  }

  /**
   * Get address
   * @return address
  */
  @NotNull
  @Schema(name = "address", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getAddress() {
    return address;
  }

  public void setNation(String nation) {
    this.nation = nation;
  }

  public BaseInfoRes ancestry(String ancestry) {
    this.ancestry = ancestry;
    return this;
  }

  /**
   * Get married
   * @return married
  */
  @NotNull
  @Schema(name = "married", requiredMode = Schema.RequiredMode.REQUIRED)
  public Boolean getMarried() {
    return married;
  }

  public void setAncestry(String ancestry) {
    this.ancestry = ancestry;
  }

  public BaseInfoRes address(String address) {
    this.address = address;
    return this;
  }

  /**
   * Get phone
   * @return phone
  */
  @NotNull
  @Schema(name = "phone", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getPhone() {
    return phone;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public BaseInfoRes married(Boolean married) {
    this.married = married;
    return this;
  }

  /**
   * Get political
   * @return political
  */
  @NotNull
  @Schema(name = "political", requiredMode = Schema.RequiredMode.REQUIRED)
  public PoliticalEnum getPolitical() {
    return political;
  }

  public void setMarried(Boolean married) {
    this.married = married;
  }

  public BaseInfoRes phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * Get org
   * @return org
  */
  @NotNull
  @Schema(name = "org", requiredMode = Schema.RequiredMode.REQUIRED)
  public JsonNullable<String> getOrg() {
    return org;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public BaseInfoRes political(PoliticalEnum political) {
    this.political = political;
    return this;
  }

  /**
   * Gets or Sets gender
   */
  public enum GenderEnum {
    MAN("男"),

    WOMAN("女");

    private final String value;

    GenderEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static GenderEnum fromValue(String value) {
      for (GenderEnum b : GenderEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  public void setPolitical(PoliticalEnum political) {
    this.political = political;
  }

  public BaseInfoRes org(String org) {
    this.org = JsonNullable.of(org);
    return this;
  }

  /**
   * Gets or Sets political
   */
  public enum PoliticalEnum {
    MASS("群众"),

    MEMBER("团员"),

    PARTY_MEMBER("党员"),

    OTHER("其他");

    private final String value;

    PoliticalEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PoliticalEnum fromValue(String value) {
      for (PoliticalEnum b : PoliticalEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  public void setOrg(JsonNullable<String> org) {
    this.org = org;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BaseInfoRes baseInfoRes = (BaseInfoRes) o;
    return Objects.equals(this.name, baseInfoRes.name) &&
        Objects.equals(this.gender, baseInfoRes.gender) &&
        Objects.equals(this.birthday, baseInfoRes.birthday) &&
        Objects.equals(this.id, baseInfoRes.id) &&
        Objects.equals(this.nation, baseInfoRes.nation) &&
        Objects.equals(this.ancestry, baseInfoRes.ancestry) &&
        Objects.equals(this.address, baseInfoRes.address) &&
        Objects.equals(this.married, baseInfoRes.married) &&
        Objects.equals(this.phone, baseInfoRes.phone) &&
        Objects.equals(this.political, baseInfoRes.political) &&
        Objects.equals(this.org, baseInfoRes.org);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, gender, birthday, id, nation, ancestry, address, married, phone, political, org);
  }

  @Override
  public String toString() {
    String sb = "class BaseInfo {\n" +
            "    name: " + toIndentedString(name) + "\n" +
            "    gender: " + toIndentedString(gender) + "\n" +
            "    birthday: " + toIndentedString(birthday) + "\n" +
            "    id: " + toIndentedString(id) + "\n" +
            "    nation: " + toIndentedString(nation) + "\n" +
            "    ancestry: " + toIndentedString(ancestry) + "\n" +
            "    address: " + toIndentedString(address) + "\n" +
            "    married: " + toIndentedString(married) + "\n" +
            "    phone: " + toIndentedString(phone) + "\n" +
            "    political: " + toIndentedString(political) + "\n" +
            "    org: " + toIndentedString(org) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

