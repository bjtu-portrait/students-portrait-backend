package bjtu.student.portrait.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * Parent
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Parent {

  @JsonProperty("name")
  private String name;

  @JsonProperty("office")
  private String office;

  @JsonProperty("career")
  private String career;

  @JsonProperty("phone")
  private String phone;

  public Parent name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  */
  @NotNull 
  @Schema(name = "name", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Parent office(String office) {
    this.office = office;
    return this;
  }

  /**
   * Get office
   * @return office
  */
  @NotNull 
  @Schema(name = "office", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getOffice() {
    return office;
  }

  public void setOffice(String office) {
    this.office = office;
  }

  public Parent career(String career) {
    this.career = career;
    return this;
  }

  /**
   * Get career
   * @return career
  */
  @NotNull 
  @Schema(name = "career", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getCareer() {
    return career;
  }

  public void setCareer(String career) {
    this.career = career;
  }

  public Parent phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * Get phone
   * @return phone
  */
  @NotNull 
  @Schema(name = "phone", requiredMode = Schema.RequiredMode.REQUIRED)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Parent parent = (Parent) o;
    return Objects.equals(this.name, parent.name) &&
        Objects.equals(this.office, parent.office) &&
        Objects.equals(this.career, parent.career) &&
        Objects.equals(this.phone, parent.phone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, office, career, phone);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Parent {\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    office: ").append(toIndentedString(office)).append("\n");
    sb.append("    career: ").append(toIndentedString(career)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

