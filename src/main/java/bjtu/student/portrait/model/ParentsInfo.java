package bjtu.student.portrait.model;

import java.net.URI;
import java.util.Objects;
import bjtu.student.portrait.model.Parent;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * ParentsInfo
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class ParentsInfo {

  @JsonProperty("father")
  private JsonNullable<Parent> father = JsonNullable.undefined();

  @JsonProperty("mother")
  private JsonNullable<Parent> mother = JsonNullable.undefined();

  public ParentsInfo father(Parent father) {
    this.father = JsonNullable.of(father);
    return this;
  }

  /**
   * Get father
   * @return father
  */
  @NotNull @Valid 
  @Schema(name = "father", requiredMode = Schema.RequiredMode.REQUIRED)
  public JsonNullable<Parent> getFather() {
    return father;
  }

  public void setFather(JsonNullable<Parent> father) {
    this.father = father;
  }

  public ParentsInfo mother(Parent mother) {
    this.mother = JsonNullable.of(mother);
    return this;
  }

  /**
   * Get mother
   * @return mother
  */
  @NotNull @Valid 
  @Schema(name = "mother", requiredMode = Schema.RequiredMode.REQUIRED)
  public JsonNullable<Parent> getMother() {
    return mother;
  }

  public void setMother(JsonNullable<Parent> mother) {
    this.mother = mother;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ParentsInfo parentsInfo = (ParentsInfo) o;
    return Objects.equals(this.father, parentsInfo.father) &&
        Objects.equals(this.mother, parentsInfo.mother);
  }

  @Override
  public int hashCode() {
    return Objects.hash(father, mother);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ParentsInfo {\n");
    sb.append("    father: ").append(toIndentedString(father)).append("\n");
    sb.append("    mother: ").append(toIndentedString(mother)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

