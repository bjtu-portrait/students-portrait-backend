package bjtu.student.portrait.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Record
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-08T21:40:28.251694500+08:00[Asia/Shanghai]")
public class Record {

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate date;

  /**
   * Get date
   * @return date
  */
  @NotNull
  @Valid
  @Schema(name = "date", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getDate() {
    return date;
  }

  @JsonProperty("type")
  private TypeEnum type;

  @JsonProperty("time")
  private JsonNullable<String> time = JsonNullable.undefined();

  public Record date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Get type
   * @return type
  */
  @NotNull
  @Schema(name = "type", requiredMode = Schema.RequiredMode.REQUIRED)
  public TypeEnum getType() {
    return type;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Record type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * Get time
   * @return time
  */
  @NotNull
  @Schema(name = "time", requiredMode = Schema.RequiredMode.REQUIRED)
  public JsonNullable<String> getTime() {
    return time;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Record time(String time) {
    this.time = JsonNullable.of(time);
    return this;
  }

  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
    LATE("晚归"),

    MISS("未归");

    private final String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String value) {
      for (TypeEnum b : TypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  public void setTime(JsonNullable<String> time) {
    this.time = time;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Record record = (Record) o;
    return Objects.equals(this.date, record.date) &&
        Objects.equals(this.type, record.type) &&
        Objects.equals(this.time, record.time);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, type, time);
  }

  @Override
  public String toString() {
    String sb = "class Record {\n" +
            "    date: " + toIndentedString(date) + "\n" +
            "    type: " + toIndentedString(type) + "\n" +
            "    time: " + toIndentedString(time) + "\n" +
            "}";
    return sb;
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

