package bjtu.student.portrait.converter;

import bjtu.student.portrait.model.HeadInfoRes;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

public class ArchiveStatusEnumConverter implements Converter<HeadInfoRes.ArchiveStatusEnum> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return HeadInfoRes.ArchiveStatusEnum.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public HeadInfoRes.ArchiveStatusEnum convertToJavaData(ReadConverterContext<?> context) {
        return HeadInfoRes.ArchiveStatusEnum.fromValue(context.getReadCellData().getStringValue());
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<HeadInfoRes.ArchiveStatusEnum> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }
}
