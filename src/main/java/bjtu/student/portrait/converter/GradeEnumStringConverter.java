package bjtu.student.portrait.converter;

import bjtu.student.portrait.model.StudentInfoRes;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

public class GradeEnumStringConverter implements Converter<StudentInfoRes.GradeEnum> {
    @Override
    public Class<?> supportJavaTypeKey() {
        return StudentInfoRes.GradeEnum.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public StudentInfoRes.GradeEnum convertToJavaData(ReadConverterContext<?> context) {
        return StudentInfoRes.GradeEnum.fromValue(context.getReadCellData().getStringValue());
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<StudentInfoRes.GradeEnum> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }
}
