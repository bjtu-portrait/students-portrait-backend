package bjtu.student.portrait.converter;

import bjtu.student.portrait.model.StudentInfoRes;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;


public class SourceEnumStringConverter implements Converter<StudentInfoRes.SourceEnum> {
    @Override
    public Class<?> supportJavaTypeKey() {
        return StudentInfoRes.SourceEnum.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public StudentInfoRes.SourceEnum convertToJavaData(ReadConverterContext<?> context) {
        return StudentInfoRes.SourceEnum.fromValue(context.getReadCellData().getStringValue());
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<StudentInfoRes.SourceEnum> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }
}
