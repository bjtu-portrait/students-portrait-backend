package bjtu.student.portrait.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

public class MarriedBooleanConverter implements Converter<Boolean> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return Boolean.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Boolean convertToJavaData(ReadConverterContext context) {
        return context.getReadCellData().getStringValue().equals("已婚");
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Boolean> context) {
        return new WriteCellData<>(context.getValue() ? "已婚" : "未婚");
    }
}
