package bjtu.student.portrait.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import org.openapitools.jackson.nullable.JsonNullable;

import java.time.LocalDate;

public class JsonNullableLocalDateConverter implements Converter<JsonNullable<LocalDate>> {
    @Override
    public Class<?> supportJavaTypeKey() {
        return JsonNullable.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public JsonNullable<LocalDate> convertToJavaData(ReadConverterContext<?> context) {
        return JsonNullable.of(LocalDate.parse(context.getReadCellData().getStringValue()));
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<JsonNullable<LocalDate>> context) {
        return new WriteCellData<>(context.getValue().get() == null ? "" : context.getValue().get().toString());
    }
}
