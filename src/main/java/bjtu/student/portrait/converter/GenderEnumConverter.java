package bjtu.student.portrait.converter;

import bjtu.student.portrait.model.BaseInfoRes;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

public class GenderEnumConverter implements Converter<BaseInfoRes.GenderEnum> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return BaseInfoRes.GenderEnum.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public BaseInfoRes.GenderEnum convertToJavaData(ReadConverterContext<?> context) {
        return BaseInfoRes.GenderEnum.fromValue(context.getReadCellData().getStringValue());
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<BaseInfoRes.GenderEnum> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }

}
