package bjtu.student.portrait.converter;

import bjtu.student.portrait.model.BaseInfoRes;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

public class PoliticalEnumConverter implements Converter<BaseInfoRes.PoliticalEnum> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return BaseInfoRes.PoliticalEnum.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public BaseInfoRes.PoliticalEnum convertToJavaData(ReadConverterContext<?> context) {
        return BaseInfoRes.PoliticalEnum.fromValue(context.getReadCellData().getStringValue());
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<BaseInfoRes.PoliticalEnum> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }
}
