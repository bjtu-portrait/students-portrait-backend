package bjtu.student.portrait.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @TableName head_info
 */
@TableName(value = "head_info")
public class HeadInfo implements Serializable {
    private Integer id;

    private String photoUrl;

    private static final long serialVersionUID = 1L;
    private String inSchool;
    private String dormWarning;
    private String studyWarning;

    private String mentor;

    private String teacher;

    private Integer stuId;
    private String archiveStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getInSchool() {
        return inSchool;
    }

    public void setInSchool(String inSchool) {
        this.inSchool = inSchool;
    }

    public String getDormWarning() {
        return dormWarning;
    }

    public void setDormWarning(String dormWarning) {
        this.dormWarning = dormWarning;
    }

    public String getStudyWarning() {
        return studyWarning;
    }

    public void setStudyWarning(String studyWarning) {
        this.studyWarning = studyWarning;
    }

    public String getArchiveStatus() {
        return archiveStatus;
    }

    public void setArchiveStatus(String archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public String getMentor() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor = mentor;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Integer getStuId() {
        return stuId;
    }

    public void setStuId(Integer stuId) {
        this.stuId = stuId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        HeadInfo other = (HeadInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getPhotoUrl() == null ? other.getPhotoUrl() == null : this.getPhotoUrl().equals(other.getPhotoUrl()))
                && (this.getInSchool() == null ? other.getInSchool() == null : this.getInSchool().equals(other.getInSchool()))
                && (this.getDormWarning() == null ? other.getDormWarning() == null : this.getDormWarning().equals(other.getDormWarning()))
                && (this.getStudyWarning() == null ? other.getStudyWarning() == null : this.getStudyWarning().equals(other.getStudyWarning()))
                && (this.getArchiveStatus() == null ? other.getArchiveStatus() == null : this.getArchiveStatus().equals(other.getArchiveStatus()))
                && (this.getMentor() == null ? other.getMentor() == null : this.getMentor().equals(other.getMentor()))
                && (this.getTeacher() == null ? other.getTeacher() == null : this.getTeacher().equals(other.getTeacher()))
                && (this.getStuId() == null ? other.getStuId() == null : this.getStuId().equals(other.getStuId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPhotoUrl() == null) ? 0 : getPhotoUrl().hashCode());
        result = prime * result + ((getInSchool() == null) ? 0 : getInSchool().hashCode());
        result = prime * result + ((getDormWarning() == null) ? 0 : getDormWarning().hashCode());
        result = prime * result + ((getStudyWarning() == null) ? 0 : getStudyWarning().hashCode());
        result = prime * result + ((getArchiveStatus() == null) ? 0 : getArchiveStatus().hashCode());
        result = prime * result + ((getMentor() == null) ? 0 : getMentor().hashCode());
        result = prime * result + ((getTeacher() == null) ? 0 : getTeacher().hashCode());
        result = prime * result + ((getStuId() == null) ? 0 : getStuId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        String sb = getClass().getSimpleName() +
                " [" +
                "Hash = " + hashCode() +
                ", id=" + id +
                ", photoUrl=" + photoUrl +
                ", inSchool=" + inSchool +
                ", dormWarning=" + dormWarning +
                ", studyWarning=" + studyWarning +
                ", archiveStatus=" + archiveStatus +
                ", mentor=" + mentor +
                ", teacher=" + teacher +
                ", stuId=" + stuId +
                ", serialVersionUID=" + serialVersionUID +
                "]";
        return sb;
    }
}
