package bjtu.student.portrait.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @TableName studies
 */
@TableName(value = "studies")
public class Studies implements Serializable {
    private Integer id;

    private String semester;

    private String lesson;

    private Double credit;

    private Double score;

    private Object isRestudy;

    private Integer stuId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Object getIsRestudy() {
        return isRestudy;
    }

    public void setIsRestudy(Object isRestudy) {
        this.isRestudy = isRestudy;
    }

    public Integer getStuId() {
        return stuId;
    }

    public void setStuId(Integer stuId) {
        this.stuId = stuId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Studies other = (Studies) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getSemester() == null ? other.getSemester() == null : this.getSemester().equals(other.getSemester()))
                && (this.getLesson() == null ? other.getLesson() == null : this.getLesson().equals(other.getLesson()))
                && (this.getCredit() == null ? other.getCredit() == null : this.getCredit().equals(other.getCredit()))
                && (this.getScore() == null ? other.getScore() == null : this.getScore().equals(other.getScore()))
                && (this.getIsRestudy() == null ? other.getIsRestudy() == null : this.getIsRestudy().equals(other.getIsRestudy()))
                && (this.getStuId() == null ? other.getStuId() == null : this.getStuId().equals(other.getStuId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSemester() == null) ? 0 : getSemester().hashCode());
        result = prime * result + ((getLesson() == null) ? 0 : getLesson().hashCode());
        result = prime * result + ((getCredit() == null) ? 0 : getCredit().hashCode());
        result = prime * result + ((getScore() == null) ? 0 : getScore().hashCode());
        result = prime * result + ((getIsRestudy() == null) ? 0 : getIsRestudy().hashCode());
        result = prime * result + ((getStuId() == null) ? 0 : getStuId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        String sb = getClass().getSimpleName() +
                " [" +
                "Hash = " + hashCode() +
                ", id=" + id +
                ", semester=" + semester +
                ", lesson=" + lesson +
                ", credit=" + credit +
                ", score=" + score +
                ", isRestudy=" + isRestudy +
                ", stuId=" + stuId +
                ", serialVersionUID=" + serialVersionUID +
                "]";
        return sb;
    }
}
