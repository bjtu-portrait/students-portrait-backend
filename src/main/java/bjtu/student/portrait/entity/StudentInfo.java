package bjtu.student.portrait.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName student_info
 */
@TableName(value = "student_info")
public class StudentInfo implements Serializable {
    private Integer id;

    private static final long serialVersionUID = 1L;

    private String name;
    private String stuNumber;

    private Date gradDate;

    private Integer dormId;
    private Integer classId;
    private String photoUrl;
    private Object inSchool;
    private Object dormWarning;
    private Object studyWarning;
    private Object archiveStatus;
    private String mentor;
    private String teacher;
    private Integer baseInfoId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStuNumber() {
        return stuNumber;
    }

    public void setStuNumber(String stuNumber) {
        this.stuNumber = stuNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Date getGradDate() {
        return gradDate;
    }

    public void setGradDate(Date gradDate) {
        this.gradDate = gradDate;
    }

    public Integer getDormId() {
        return dormId;
    }

    public void setDormId(Integer dormId) {
        this.dormId = dormId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getInSchool() {
        return inSchool;
    }

    public void setInSchool(Object inSchool) {
        this.inSchool = inSchool;
    }

    public Object getDormWarning() {
        return dormWarning;
    }

    public void setDormWarning(Object dormWarning) {
        this.dormWarning = dormWarning;
    }

    public Object getStudyWarning() {
        return studyWarning;
    }

    public void setStudyWarning(Object studyWarning) {
        this.studyWarning = studyWarning;
    }

    public Object getArchiveStatus() {
        return archiveStatus;
    }

    public void setArchiveStatus(Object archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public String getMentor() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor = mentor;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Integer getBaseInfoId() {
        return baseInfoId;
    }

    public void setBaseInfoId(Integer baseInfoId) {
        this.baseInfoId = baseInfoId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        StudentInfo other = (StudentInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getStuNumber() == null ? other.getStuNumber() == null : this.getStuNumber().equals(other.getStuNumber()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getClassId() == null ? other.getClassId() == null : this.getClassId().equals(other.getClassId()))
                && (this.getGradDate() == null ? other.getGradDate() == null : this.getGradDate().equals(other.getGradDate()))
                && (this.getDormId() == null ? other.getDormId() == null : this.getDormId().equals(other.getDormId()))
                && (this.getPhotoUrl() == null ? other.getPhotoUrl() == null : this.getPhotoUrl().equals(other.getPhotoUrl()))
                && (this.getInSchool() == null ? other.getInSchool() == null : this.getInSchool().equals(other.getInSchool()))
                && (this.getDormWarning() == null ? other.getDormWarning() == null : this.getDormWarning().equals(other.getDormWarning()))
                && (this.getStudyWarning() == null ? other.getStudyWarning() == null : this.getStudyWarning().equals(other.getStudyWarning()))
                && (this.getArchiveStatus() == null ? other.getArchiveStatus() == null : this.getArchiveStatus().equals(other.getArchiveStatus()))
                && (this.getMentor() == null ? other.getMentor() == null : this.getMentor().equals(other.getMentor()))
                && (this.getTeacher() == null ? other.getTeacher() == null : this.getTeacher().equals(other.getTeacher()))
                && (this.getBaseInfoId() == null ? other.getBaseInfoId() == null : this.getBaseInfoId().equals(other.getBaseInfoId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getStuNumber() == null) ? 0 : getStuNumber().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getClassId() == null) ? 0 : getClassId().hashCode());
        result = prime * result + ((getGradDate() == null) ? 0 : getGradDate().hashCode());
        result = prime * result + ((getDormId() == null) ? 0 : getDormId().hashCode());
        result = prime * result + ((getPhotoUrl() == null) ? 0 : getPhotoUrl().hashCode());
        result = prime * result + ((getInSchool() == null) ? 0 : getInSchool().hashCode());
        result = prime * result + ((getDormWarning() == null) ? 0 : getDormWarning().hashCode());
        result = prime * result + ((getStudyWarning() == null) ? 0 : getStudyWarning().hashCode());
        result = prime * result + ((getArchiveStatus() == null) ? 0 : getArchiveStatus().hashCode());
        result = prime * result + ((getMentor() == null) ? 0 : getMentor().hashCode());
        result = prime * result + ((getTeacher() == null) ? 0 : getTeacher().hashCode());
        result = prime * result + ((getBaseInfoId() == null) ? 0 : getBaseInfoId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        String sb = getClass().getSimpleName() +
                " [" +
                "Hash = " + hashCode() +
                ", id=" + id +
                ", stuNumber=" + stuNumber +
                ", name=" + name +
                ", classId=" + classId +
                ", gradDate=" + gradDate +
                ", dormId=" + dormId +
                ", photoUrl=" + photoUrl +
                ", inSchool=" + inSchool +
                ", dormWarning=" + dormWarning +
                ", studyWarning=" + studyWarning +
                ", archieveStatus=" + archiveStatus +
                ", mentor=" + mentor +
                ", teacher=" + teacher +
                ", baseInfoId=" + baseInfoId +
                ", serialVersionUID=" + serialVersionUID +
                "]";
        return sb;
    }
}
