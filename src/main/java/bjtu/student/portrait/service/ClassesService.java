package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Classes;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【classes】的数据库操作Service
 * @createDate 2023-04-08 22:44:53
 */
public interface ClassesService extends IService<Classes> {
    List<String> getClasses(String major);
}
