package bjtu.student.portrait.service;

import bjtu.student.portrait.dto.StudentPageDTO;
import bjtu.student.portrait.entity.StudentInfo;
import bjtu.student.portrait.model.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【student_info】的数据库操作Service
 * @createDate 2023-03-10 14:52:56
 */
public interface StudentInfoService extends IService<StudentInfo> {

    StuIdRes getIdByName(String name);

    StudentPage getStudentPage(StudentPageDTO dto);

    List<String> getStudentNumsByStudentPage(StudentPageDTO dto);

//    List<String> getMajors();
//
//    List<String> getClasses(String major);

    Integer getTrueId(String stuId);

    StudentInfoRes getStudentInfo(String stuId);

    HeadInfoRes getHeadInfo(String stuId);

    List<Dormmate> getDormmateByStuNum(String stuNum);

    List<ExportRes> getExportData(List<String> stuNums);
}
