package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Dorms;
import bjtu.student.portrait.model.Dorm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【dorms】的数据库操作Service
 * @createDate 2023-03-11 22:33:47
 */
public interface DormsService extends IService<Dorms> {
    Dorm getDormByStuNum(String stuNum);
}
