package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Jobs;
import bjtu.student.portrait.model.Job;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【jobs】的数据库操作Service
 * @createDate 2023-03-11 22:33:59
 */
public interface JobsService extends IService<Jobs> {
    Job getJobByStuNum(String stuNum);
}
