package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Counsellings;
import bjtu.student.portrait.model.CounsellingRes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【counsellings】的数据库操作Service
 * @createDate 2023-03-11 22:33:37
 */
public interface CounsellingsService extends IService<Counsellings> {
    CounsellingRes getCounsellingByStuNum(String stuNum);

}
