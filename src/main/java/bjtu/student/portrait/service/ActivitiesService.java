package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Activities;
import bjtu.student.portrait.model.ActivityRes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【activities】的数据库操作Service
 * @createDate 2023-03-11 22:30:46
 */
public interface ActivitiesService extends IService<Activities> {
    ActivityRes getActivityByStuNum(String stuNum);
}
