package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Appraisals;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【appraisals】的数据库操作Service
 * @createDate 2023-03-11 22:33:20
 */
public interface AppraisalsService extends IService<Appraisals> {

}
