package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Punishments;
import bjtu.student.portrait.mapper.PunishmentsMapper;
import bjtu.student.portrait.model.Punishment;
import bjtu.student.portrait.service.PunishmentsService;
import bjtu.student.portrait.service.StudentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【punishments】的数据库操作Service实现
 * @createDate 2023-03-11 22:34:15
 */
@Service
public class PunishmentsServiceImpl extends ServiceImpl<PunishmentsMapper, Punishments>
        implements PunishmentsService {
    private final StudentInfoService studentInfoService;

    public PunishmentsServiceImpl(StudentInfoService studentInfoService) {
        this.studentInfoService = studentInfoService;
    }

    @Override
    public List<Punishment> getPunishmentsByStuNum(String stuNum) {
        Integer stuId = studentInfoService.getTrueId(stuNum);
        return this.baseMapper.selectPunishmentsByStuId(stuId);
    }
}




