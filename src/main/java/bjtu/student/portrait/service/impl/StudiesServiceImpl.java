package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Studies;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.mapper.StudiesMapper;
import bjtu.student.portrait.model.Study;
import bjtu.student.portrait.service.StudiesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【studies】的数据库操作Service实现
 * @createDate 2023-03-11 22:34:47
 */

@Service
public class StudiesServiceImpl extends ServiceImpl<StudiesMapper, Studies>
        implements StudiesService {
    @Autowired
    private StudiesMapper studiesMapper;
    @Autowired
    private StudentInfoMapper studentInfoMapper;

    public List<Study> getStudiesByStuNum(String stuNum) {
        Integer id = studentInfoMapper.getTrueId(stuNum);
        return studiesMapper.getStudiesByStuId(id);
    }
}




