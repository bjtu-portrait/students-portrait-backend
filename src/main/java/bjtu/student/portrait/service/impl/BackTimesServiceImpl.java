package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.BackTimes;
import bjtu.student.portrait.mapper.BackTimesMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Average;
import bjtu.student.portrait.model.BackTime;
import bjtu.student.portrait.model.Record;
import bjtu.student.portrait.model.Statistic;
import bjtu.student.portrait.service.BackTimesService;
import bjtu.student.portrait.util.DateUtil;
import bjtu.student.portrait.util.Semester;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Darley
 * @description 针对表【back_times】的数据库操作Service实现
 * @createDate 2023-03-11 22:33:25
 */
@Service
public class BackTimesServiceImpl extends ServiceImpl<BackTimesMapper, BackTimes>
        implements BackTimesService {
    private final StudentInfoMapper studentInfoMapper;

    public BackTimesServiceImpl(StudentInfoMapper studentInfoMapper) {
        this.studentInfoMapper = studentInfoMapper;
    }

    @Override
    public Statistic getStatisticByStuNum(String stuNum) {
        Semester semester = DateUtil.getSemester();
        LocalDate startDate = semester.startDate();
        LocalDate endDate = semester.endDate();
        Statistic statistic = new Statistic();
        Integer stuId = studentInfoMapper.getTrueId(stuNum);
        String averageBackTime = baseMapper.selectSemesterAverageBackTimeByStuId(stuId, startDate, endDate);
        List<BackTime> backTimes = baseMapper.listSemesterBackTimesByStuId(stuId, startDate, endDate);
        List<Average> averages = baseMapper.listAveragesByStuId(stuId);
        List<Record> records = baseMapper.listUnNormalRecordsByStuId(stuId);
        return statistic.average(averageBackTime).times(backTimes).averages(averages).records(records);
    }
}




