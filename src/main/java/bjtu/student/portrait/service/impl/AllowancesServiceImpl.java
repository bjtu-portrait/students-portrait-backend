package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Allowances;
import bjtu.student.portrait.mapper.AllowancesMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Allowance;
import bjtu.student.portrait.service.AllowancesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【allowances】的数据库操作Service实现
 * @createDate 2023-03-11 22:33:09
 */
@Service
public class AllowancesServiceImpl extends ServiceImpl<AllowancesMapper, Allowances>
        implements AllowancesService {

    private final StudentInfoMapper studentInfoMapper;

    @Autowired
    public AllowancesServiceImpl(StudentInfoMapper studentInfoMapper) {
        this.studentInfoMapper = studentInfoMapper;
    }

    @Override
    public List<Allowance> getAllowancesByStuNum(String stuNum) {
        Integer stuId = studentInfoMapper.selectIdByStuNumber(stuNum);
        return this.baseMapper.selectAllowancesByStuId(stuId);
    }
}




