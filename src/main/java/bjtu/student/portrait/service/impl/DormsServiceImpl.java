package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Dorms;
import bjtu.student.portrait.mapper.AppraisalsMapper;
import bjtu.student.portrait.mapper.DormsMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Appraisal;
import bjtu.student.portrait.model.Dorm;
import bjtu.student.portrait.service.DormsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【dorms】的数据库操作Service实现
 * @createDate 2023-03-11 22:33:47
 */
@Service
public class DormsServiceImpl extends ServiceImpl<DormsMapper, Dorms>
        implements DormsService {
    private final StudentInfoMapper studentInfoMapper;
    private final AppraisalsMapper appraisalsMapper;

    public DormsServiceImpl(StudentInfoMapper studentInfoMapper, AppraisalsMapper appraisalsMapper) {
        this.studentInfoMapper = studentInfoMapper;
        this.appraisalsMapper = appraisalsMapper;
    }

    @Override
    public Dorm getDormByStuNum(String stuNum) {
        Integer dormId = studentInfoMapper.selectOneByStuNumber(stuNum).getDormId();
        Dorms dorms = baseMapper.selectOneById(dormId);
        Dorm dorm = new Dorm();
        dorm.building(dorms.getBuilding()).room(dorms.getRoom());
        List<Appraisal> appraisals = appraisalsMapper.listAppraisalsByDormId(dormId);
        dorm.appraisals(appraisals);
        return dorm;
    }
}




