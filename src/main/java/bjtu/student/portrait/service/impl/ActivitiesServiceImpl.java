package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Activities;
import bjtu.student.portrait.mapper.ActivitiesMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Activity;
import bjtu.student.portrait.model.ActivityRes;
import bjtu.student.portrait.service.ActivitiesService;
import bjtu.student.portrait.util.DateUtil;
import bjtu.student.portrait.util.Semester;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【activities】的数据库操作Service实现
 * @createDate 2023-03-11 22:30:46
 */
@Service
public class ActivitiesServiceImpl extends ServiceImpl<ActivitiesMapper, Activities>
        implements ActivitiesService {
    private final StudentInfoMapper studentInfoMapper;

    public ActivitiesServiceImpl(StudentInfoMapper studentInfoMapper) {
        this.studentInfoMapper = studentInfoMapper;
    }

    @Override
    public ActivityRes getActivityByStuNum(String stuNum) {
        Integer stuId = studentInfoMapper.selectOneByStuNumber(stuNum).getId();
        Semester semester = DateUtil.getSemester();
        Integer semesterAmount = baseMapper.selectSemesterAmountByStuId(stuId, semester.startDate(), semester.endDate());
        Integer semesterTally = baseMapper.selectSemesterTallyByStuId(stuId, semester.startDate(), semester.endDate());
        List<Activity> activities = baseMapper.listActivitiesByStuId(stuId);
        return new ActivityRes().semesterAmount(semesterAmount).semesterTally(semesterTally).activities(activities);
    }

}




