package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.dto.StudentPageDTO;
import bjtu.student.portrait.entity.StudentInfo;
import bjtu.student.portrait.exception.NotFoundException;
import bjtu.student.portrait.mapper.BaseInfoMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.*;
import bjtu.student.portrait.service.StudentInfoService;
import bjtu.student.portrait.util.Pack;
import com.alibaba.excel.util.ListUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【student_info】的数据库操作Service实现
 * @createDate 2023-03-10 14:52:56
 */
@Service
public class StudentInfoServiceImpl extends ServiceImpl<StudentInfoMapper, StudentInfo>
        implements StudentInfoService {
    private final BaseInfoMapper baseInfoMapper;

    public StudentInfoServiceImpl(BaseInfoMapper baseInfoMapper) {
        this.baseInfoMapper = baseInfoMapper;
    }

    @Override
    public StuIdRes getIdByName(String name) {
        var res = new StuIdRes();
        var list = this.list(new LambdaQueryWrapper<StudentInfo>().like(StudentInfo::getName, "%" + name + "%"));
        switch (list.size()) {
            case 0 -> res.setType(StuIdRes.TypeEnum.NUMBER_3);
            case 1 -> {
                res.setType(StuIdRes.TypeEnum.NUMBER_1);
                res.setStuId(list.get(0).getStuNumber());
            }
            default -> res.setType(StuIdRes.TypeEnum.NUMBER_2);
        }
        return res;
    }


    @Override
    public StudentPage getStudentPage(StudentPageDTO dto) {
        var studentPage = this.baseMapper.getStudentPage(new Page<>(dto.getPage(), dto.getSize()), dto);
        return Pack.studentPage(studentPage);
    }

    @Override
    public List<String> getStudentNumsByStudentPage(StudentPageDTO dto) {
        return this.baseMapper.getStuNumsByStudentPage(dto);
    }

//    @Override
//    public List<String> getMajors() {
//        return studentInfoMapper.getMajors();
//    }
//
//    @Override
//    public List<String> getClasses(String major) {
//        return studentInfoMapper.getClasses(major);
//    }


    @Override
    public Integer getTrueId(String stuId) {
        var result = this.lambdaQuery().eq(StudentInfo::getStuNumber, stuId);
        if (result.count() == 0) {
            throw new NotFoundException("学号不存在");
        }
        return result.one().getId();
    }

    @Override
    public StudentInfoRes getStudentInfo(String stuId) {
        return this.baseMapper.getStudentInfo(stuId);
    }

    @Override
    public HeadInfoRes getHeadInfo(String stuId) {
        return this.baseMapper.getHeadInfo(stuId);
    }

    @Override
    public List<Dormmate> getDormmateByStuNum(String stuNum) {
        return this.baseMapper.getDormmateByStuNum(stuNum);
    }

    @Override
    public List<ExportRes> getExportData(List<String> stuNums) {
        List<StudentInfoRes> studentInfoResList = this.baseMapper.getStudentInfoByStuNums(stuNums);
        List<HeadInfoRes> headInfoResList = this.baseMapper.getHeadInfoByStuNums(stuNums);
        List<BaseInfoRes> baseInfoResList = this.baseInfoMapper.getBaseInfoByStuNums(stuNums);
        List<ExportRes> exportResList = ListUtils.newArrayList();
        for (int i = 0; i < studentInfoResList.size(); i++) {
            ExportRes exportRes = new ExportRes();
            BeanUtils.copyProperties(studentInfoResList.get(i), exportRes);
            BeanUtils.copyProperties(headInfoResList.get(i), exportRes);
            BeanUtils.copyProperties(baseInfoResList.get(i), exportRes);
            exportResList.add(exportRes);
        }
        return exportResList;
    }
}




