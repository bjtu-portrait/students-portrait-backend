package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Classes;
import bjtu.student.portrait.mapper.ClassesMapper;
import bjtu.student.portrait.mapper.MajorsMapper;
import bjtu.student.portrait.service.ClassesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【classes】的数据库操作Service实现
 * @createDate 2023-04-08 22:44:53
 */
@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes>
        implements ClassesService {

    private final MajorsMapper majorsMapper;

    @Autowired
    public ClassesServiceImpl(MajorsMapper majorsMapper) {
        this.majorsMapper = majorsMapper;
    }

    @Override
    public List<String> getClasses(String major) {
        List<Integer> majorIds = this.majorsMapper.getIdsByName(major);
        return this.baseMapper.getClasses(majorIds);
    }
}




