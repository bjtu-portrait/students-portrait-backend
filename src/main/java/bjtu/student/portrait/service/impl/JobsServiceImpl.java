package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Jobs;
import bjtu.student.portrait.mapper.JobsMapper;
import bjtu.student.portrait.model.Job;
import bjtu.student.portrait.model.JobTime;
import bjtu.student.portrait.service.JobsService;
import bjtu.student.portrait.service.StudentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【jobs】的数据库操作Service实现
 * @createDate 2023-03-11 22:33:59
 */
@Service
public class JobsServiceImpl extends ServiceImpl<JobsMapper, Jobs>
        implements JobsService {
    private final StudentInfoService studentInfoService;

    public JobsServiceImpl(StudentInfoService studentInfoService) {
        this.studentInfoService = studentInfoService;
    }

    @Override
    public Job getJobByStuNum(String stuNum) {
        Integer stuId = studentInfoService.getTrueId(stuNum);
        Integer amount = this.baseMapper.countNameByStuId(stuId);
        List<JobTime> jobTimes = this.baseMapper.selectByStuIdGroupByYear(stuId);
        return new Job().amount(amount).jobTimes(jobTimes);
    }
}




