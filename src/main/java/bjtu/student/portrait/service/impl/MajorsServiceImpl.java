package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Majors;
import bjtu.student.portrait.mapper.MajorsMapper;
import bjtu.student.portrait.service.MajorsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【majors】的数据库操作Service实现
 * @createDate 2023-04-08 22:45:16
 */
@Service
public class MajorsServiceImpl extends ServiceImpl<MajorsMapper, Majors>
        implements MajorsService {

    @Override
    public List<String> getMajors() {
        return this.baseMapper.getMajors();
    }
}




