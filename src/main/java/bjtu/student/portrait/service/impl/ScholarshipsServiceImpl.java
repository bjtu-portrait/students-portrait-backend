package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Scholarships;
import bjtu.student.portrait.mapper.ScholarshipsMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Scholarship;
import bjtu.student.portrait.service.ScholarshipsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【scholarships】的数据库操作Service实现
 * @createDate 2023-03-11 22:34:36
 */
@Service
public class ScholarshipsServiceImpl extends ServiceImpl<ScholarshipsMapper, Scholarships>
        implements ScholarshipsService {

    private final StudentInfoMapper studentInfoMapper;

    @Autowired
    public ScholarshipsServiceImpl(StudentInfoMapper studentInfoMapper) {
        this.studentInfoMapper = studentInfoMapper;
    }

    @Override
    public List<Scholarship> getScholarshipsByStuNum(String stuNum) {
        Integer stuId = studentInfoMapper.getTrueId(stuNum);
        return baseMapper.selectScholarshipsByStuId(stuId);
    }
}




