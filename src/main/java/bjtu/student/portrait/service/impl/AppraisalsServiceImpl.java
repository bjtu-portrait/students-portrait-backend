package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Appraisals;
import bjtu.student.portrait.mapper.AppraisalsMapper;
import bjtu.student.portrait.service.AppraisalsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author Darley
 * @description 针对表【appraisals】的数据库操作Service实现
 * @createDate 2023-03-11 22:33:20
 */
@Service
public class AppraisalsServiceImpl extends ServiceImpl<AppraisalsMapper, Appraisals>
        implements AppraisalsService {

}




