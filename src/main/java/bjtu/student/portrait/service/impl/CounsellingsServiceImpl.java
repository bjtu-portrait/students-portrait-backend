package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Counsellings;
import bjtu.student.portrait.mapper.CounsellingsMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Counselling;
import bjtu.student.portrait.model.CounsellingRes;
import bjtu.student.portrait.service.CounsellingsService;
import bjtu.student.portrait.util.DateUtil;
import bjtu.student.portrait.util.Semester;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【counsellings】的数据库操作Service实现
 * @createDate 2023-03-11 22:33:37
 */
@Service
public class CounsellingsServiceImpl extends ServiceImpl<CounsellingsMapper, Counsellings>
        implements CounsellingsService {
    private final StudentInfoMapper studentInfoMapper;

    public CounsellingsServiceImpl(StudentInfoMapper studentInfoMapper) {
        this.studentInfoMapper = studentInfoMapper;
    }

    @Override
    public CounsellingRes getCounsellingByStuNum(String stuNum) {
        Integer stuId = studentInfoMapper.selectOneByStuNumber(stuNum).getId();
        Semester semester = DateUtil.getSemester();
        Integer semesterAmount = baseMapper.selectSemesterAmountByStuId(stuId, semester.startDate(), semester.endDate());
        List<Counselling> counsellings = baseMapper.listCounsellingsByStuId(stuId);
        return new CounsellingRes().semesterAmount(semesterAmount).counsellings(counsellings);
    }

}




