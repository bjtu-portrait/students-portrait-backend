package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Parents;
import bjtu.student.portrait.mapper.ParentsMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.ParentsInfo;
import bjtu.student.portrait.service.ParentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Darley
 * @description 针对表【parents】的数据库操作Service实现
 * @createDate 2023-03-11 22:34:10
 */
@Service
public class ParentsServiceImpl extends ServiceImpl<ParentsMapper, Parents>
        implements ParentsService {

    @Autowired
    private StudentInfoMapper studentInfoMapper;

    public ParentsInfo getParents(String stuId) {
        var father = baseMapper.getFather(stuId);
        var mother = baseMapper.getMother(stuId);
        return new ParentsInfo().father(father).mother(mother);
    }

}




