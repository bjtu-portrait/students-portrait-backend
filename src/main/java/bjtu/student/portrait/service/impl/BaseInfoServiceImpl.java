package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.BaseInfo;
import bjtu.student.portrait.mapper.BaseInfoMapper;
import bjtu.student.portrait.model.BaseInfoRes;
import bjtu.student.portrait.service.BaseInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author Darley
 * @description 针对表【base_info】的数据库操作Service实现
 * @createDate 2023-03-09 18:04:36
 */


@Service
public class BaseInfoServiceImpl extends ServiceImpl<BaseInfoMapper, BaseInfo>
        implements BaseInfoService {

    public BaseInfoRes getBaseInfo(String stuId) {
        return baseMapper.getBaseInfo(stuId);
    }

}




