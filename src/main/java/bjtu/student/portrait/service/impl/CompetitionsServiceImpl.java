package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Competitions;
import bjtu.student.portrait.mapper.CompetitionsMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Competition;
import bjtu.student.portrait.service.CompetitionsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【competitions】的数据库操作Service实现
 * @createDate 2023-03-11 22:33:32
 */
@Service
public class CompetitionsServiceImpl extends ServiceImpl<CompetitionsMapper, Competitions>
        implements CompetitionsService {
    private final StudentInfoMapper studentInfoMapper;

    @Autowired
    public CompetitionsServiceImpl(StudentInfoMapper studentInfoMapper) {
        this.studentInfoMapper = studentInfoMapper;
    }

    @Override
    public List<Competition> getCompetitionsByStuNum(String stuNum) {
        Integer stuId = studentInfoMapper.selectIdByStuNumber(stuNum);
        return this.baseMapper.selectCompetitionsByStuId(stuId);
    }
}




