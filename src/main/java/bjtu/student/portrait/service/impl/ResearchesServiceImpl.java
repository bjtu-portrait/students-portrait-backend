package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Researches;
import bjtu.student.portrait.mapper.ResearchesMapper;
import bjtu.student.portrait.mapper.StudentInfoMapper;
import bjtu.student.portrait.model.Research;
import bjtu.student.portrait.service.ResearchesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【researches】的数据库操作Service实现
 * @createDate 2023-03-11 22:34:29
 */
@Service
public class ResearchesServiceImpl extends ServiceImpl<ResearchesMapper, Researches>
        implements ResearchesService {
    @Autowired
    private ResearchesMapper researchesMapper;
    @Autowired
    private StudentInfoMapper studentInfoMapper;

    public List<Research> getResearchesByStuNum(String stuNum) {
        Integer id = studentInfoMapper.getTrueId(stuNum);
        return researchesMapper.getResearchesByStuId(id);
    }
}




