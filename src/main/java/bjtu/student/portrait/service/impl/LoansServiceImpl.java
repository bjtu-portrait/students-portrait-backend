package bjtu.student.portrait.service.impl;

import bjtu.student.portrait.entity.Loans;
import bjtu.student.portrait.mapper.LoansMapper;
import bjtu.student.portrait.model.Loan;
import bjtu.student.portrait.model.LoanRes;
import bjtu.student.portrait.service.LoansService;
import bjtu.student.portrait.service.StudentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【loans】的数据库操作Service实现
 * @createDate 2023-03-11 22:34:05
 */
@Service
public class LoansServiceImpl extends ServiceImpl<LoansMapper, Loans>
        implements LoansService {
    private final StudentInfoService studentInfoService;

    @Autowired
    public LoansServiceImpl(StudentInfoService studentInfoService) {
        this.studentInfoService = studentInfoService;
    }

    @Override
    public LoanRes getLoanByStuNum(String stuNum) {
        Integer stuId = studentInfoService.getTrueId(stuNum);
        Integer repayAmount = this.baseMapper.selectRepayAmountByStuId(stuId);
        List<Loan> loans = this.baseMapper.selectLoansByStuId(stuId);
        return new LoanRes().repayAmount(repayAmount).loans(loans);
    }
}




