package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.BaseInfo;
import bjtu.student.portrait.model.BaseInfoRes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【base_info】的数据库操作Service
 * @createDate 2023-03-09 18:04:36
 */
public interface BaseInfoService extends IService<BaseInfo> {
    BaseInfoRes getBaseInfo(String stuId);
}
