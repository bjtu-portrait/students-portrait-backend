package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Scholarships;
import bjtu.student.portrait.model.Scholarship;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【scholarships】的数据库操作Service
 * @createDate 2023-03-11 22:34:36
 */
public interface ScholarshipsService extends IService<Scholarships> {
    List<Scholarship> getScholarshipsByStuNum(String stuNum);
}
