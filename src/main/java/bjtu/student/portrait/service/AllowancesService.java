package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Allowances;
import bjtu.student.portrait.model.Allowance;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【allowances】的数据库操作Service
 * @createDate 2023-03-11 22:33:09
 */
public interface AllowancesService extends IService<Allowances> {
    List<Allowance> getAllowancesByStuNum(String stuNum);
}
