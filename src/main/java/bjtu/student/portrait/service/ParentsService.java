package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Parents;
import bjtu.student.portrait.model.BaseInfoRes;
import bjtu.student.portrait.model.ParentsInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【parents】的数据库操作Service
 * @createDate 2023-03-11 22:34:10
 */
public interface ParentsService extends IService<Parents> {
    ParentsInfo getParents(String stuId);
}
