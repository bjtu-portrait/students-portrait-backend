package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Competitions;
import bjtu.student.portrait.model.Competition;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【competitions】的数据库操作Service
 * @createDate 2023-03-11 22:33:32
 */
public interface CompetitionsService extends IService<Competitions> {
    List<Competition> getCompetitionsByStuNum(String stuNum);
}
