package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Researches;
import bjtu.student.portrait.model.Research;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【researches】的数据库操作Service
 * @createDate 2023-03-11 22:34:29
 */
public interface ResearchesService extends IService<Researches> {
    List<Research> getResearchesByStuNum(String stuNum);
}
