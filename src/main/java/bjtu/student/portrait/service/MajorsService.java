package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Majors;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【majors】的数据库操作Service
 * @createDate 2023-04-08 22:45:16
 */
public interface MajorsService extends IService<Majors> {
    List<String> getMajors();
}
