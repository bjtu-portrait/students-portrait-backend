package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Punishments;
import bjtu.student.portrait.model.Punishment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【punishments】的数据库操作Service
 * @createDate 2023-03-11 22:34:15
 */
public interface PunishmentsService extends IService<Punishments> {
    List<Punishment> getPunishmentsByStuNum(String stuNum);
}
