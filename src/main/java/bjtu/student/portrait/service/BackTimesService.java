package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.BackTimes;
import bjtu.student.portrait.model.Statistic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【back_times】的数据库操作Service
 * @createDate 2023-03-11 22:33:25
 */
public interface BackTimesService extends IService<BackTimes> {
    Statistic getStatisticByStuNum(String stuNum);
}
