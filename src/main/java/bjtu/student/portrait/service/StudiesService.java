package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Studies;
import bjtu.student.portrait.model.Study;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Darley
 * @description 针对表【studies】的数据库操作Service
 * @createDate 2023-03-11 22:34:47
 */
public interface StudiesService extends IService<Studies> {
    List<Study> getStudiesByStuNum(String stuNum);
}
