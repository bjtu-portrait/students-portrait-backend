package bjtu.student.portrait.service;

import bjtu.student.portrait.entity.Loans;
import bjtu.student.portrait.model.LoanRes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Darley
 * @description 针对表【loans】的数据库操作Service
 * @createDate 2023-03-11 22:34:05
 */
public interface LoansService extends IService<Loans> {
    LoanRes getLoanByStuNum(String stuNum);
}
