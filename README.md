# students-portrait-backend

#### 介绍

学生画像后端

#### 软件架构

软件架构说明

#### 安装教程

1. `apt install openjdk-17-jdk-headless`
2. `apt install maven`
3. xxxx

#### 使用说明

1. 使用项目远程 MySQL 数据库，需将 ssh 公钥添加到数据库服务器的 `~/.ssh/authorized_keys` 文件中
2. 使用 `mvn spring-boot:run` 启动项目
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 xxx 分支
3. 提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
